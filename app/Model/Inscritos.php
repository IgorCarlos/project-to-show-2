<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Inscritos extends Model
{
    protected $table = "inscritos";
    protected $fillable = ['idUnidade','idEvento','dia','confirmado','nome','email','telefone','documento'];

    public function unidades(){
        return $this->belongsTo('App\Model\Unidade', 'idUnidade');
    }

    public function eventos(){
        return $this->hasOne('App\Model\Evento');
    }
}
