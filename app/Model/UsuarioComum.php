<?php

namespace App\Model;

use App\Model\Usuario;
use Illuminate\Database\Eloquent\Model;


class UsuarioComum extends Usuario
{

    protected $table = 'usuarios_comum';
    protected $primaryKey = 'usuarioId';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document', 'dateOfBirth', 'registration', 'state', 'city', 'newsletter', 'phone', 'tabVideo'
    ];

    protected $hidden = [
        'usuarioId'
    ];

    public function usuario () {
        return $this->belongsTo('App\Model\Usuario', 'usuarioId');
    }

    public function token () {
        return $this->hasOne('App\Model\Token', 'usuarioComumId');
    }

    public function tp_resposta(){
        return $this->hasOne('App\Model\TrilhaPremium', 'idUsuario');
    }

    public function cuboPos () {
        return $this->hasOne('App\Model\CuboPosAtivo', 'NR_CPF', 'document');
    }

}




