<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ArtigoTag extends Model
{
    protected $table = 'artigos_tag';
    public $timestamps = false;
    public $fillable = [
        'tagId', 'artigoId'
    ];

}
