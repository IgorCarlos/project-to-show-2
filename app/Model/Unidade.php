<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    protected $table = "unidades";

    protected $fillable = ['nome','endereco','cidade','bairro','estado','numero','idPolo'];

    public function polo(){
        return $this->belongsTo('App\Model\Polo','idPolo');
    }

    protected $hidden = ['senha'];
}


/*$('.tb_regs tr').each(function(){
    if($(this).find('td').eq(1).text() == "Anhanguera"){
        var idPolo = 3;
    }else if($(this).find('td').eq(1).text() == "Fama"){
        var idPolo = 4;
    }else if($(this).find('td').eq(1).text() == "Pitágoras"){
        var idPolo = 5;
    }else if($(this).find('td').eq(1).text() == "Unic"){
        var idPolo = 6;
    }else if($(this).find('td').eq(1).text() == "Uniderp"){
        var idPolo = 7;
    }else if($(this).find('td').eq(1).text() == "Unime"){
        var idPolo = 8;
    }else if($(this).find('td').eq(1).text() == "Unopar"){
        var idPolo = 9;
    }
​
    var cidade = $(this).find('td').eq(2).text().split('/')[0];
    var estado = $(this).find('td').eq(2).text().split('/')[1];

    if(typeof(estado) != 'undefined'){
        if(estado.indexOf('-') != -1){
            estado = estado.replace('-')[0];
        }

    }
​
    console.log(estado);
    //$('body').append(idPolo + ',' + $(this).find('td').eq(1).text() . '<br/>');
});*/