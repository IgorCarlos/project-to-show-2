<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Polo extends Model
{
    protected $table = "polos";
    protected $fillable = ['nomePolo','logo'];

    public function unidades(){
        return $this->hasMany('App\Model\Unidade','idPolo', 'id');
    }
}
