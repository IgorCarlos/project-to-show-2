<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsuarioAdm extends Usuario
{
    protected $table = 'usuarios_adm';
    protected $primaryKey = 'usuarioId';
    public $timestamps = false;

    public $fillable = ['usuarioId'];

    public function usuario () {
        return $this->belongsTo('App\Model\Usuario', 'usuarioId');
    }

}
