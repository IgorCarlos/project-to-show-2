<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = "evento";
    protected $fillable = ['descricao','mes','datainicio','datafim','nomepalestrante','descricaopalestrante','linkvideo','status'];

    public function unidades(){
        return $this->belongsToMany('App\Model\Unidade','evento_has_unidade', 'eventoId', 'unidadeId')->withPivot('dia_evento', 'hora_evento');
    }

    public function inscritos(){
        return $this->hasMany('App\Model\Inscritos', 'idEvento');
    }
}
