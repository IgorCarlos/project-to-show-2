<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TrilhaPremium extends Model
{
    protected $table = "tp_respostas";

    protected $fillable = ['etapa','desafioUp','termos_materiais','termos_relacionais','termos_mentais','termos_sociais','parar_fazer','continuar_fazendo','comecar_fazer','idUsuario'];


    public function usuario(){
        return $this->belongsTo('App/Model/UsuarioComum', 'idUsuario');
    }
}
