<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Token extends Model
{
    
    public $table = 'tokens';
    public $primaryKey = 'hash';
    public $incrementing = false;

    const TIME_TOKEN = '+2 hours';

    public $fillable = [
        'hash', 'usuarioComumId', 'tempoAcesso', 'created_at', 'updated_at'
    ];

    public function usuarioComum () {
        return $this->belongsTo('App\Model\UsuarioComum', 'usuarioComumId');
    }

    public function renew () {
        $this->tempoAcesso = date('Y-m-d H:i:s', strtotime(self::TIME_TOKEN));
        $this->save();
    }

    public static function generate () {
        $token = [];

        $token['hash'] = sha1(strtotime('now'));
        $token['tempoAcesso'] = date('Y-m-d H:i:s', strtotime(self::TIME_TOKEN));

        return $token;
    }

    public static function isValid ($usuarioToken) {
        if (strtotime($usuarioToken->tempoAcesso) < strtotime(date('Y-m-d H:i:s'))) throw new \Exception('Token expirado', 400);
    }

}
