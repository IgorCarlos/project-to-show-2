<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{
    protected $table = "pos";

    protected $fillable = ['nome'];

    public function cursos()
    {
        return $this->belongsToMany('App\Model\Cursos','curso_has_pos', 'id_pos', 'id_curso');
    }
}
