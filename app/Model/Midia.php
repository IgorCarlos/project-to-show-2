<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Midia extends Model
{
    protected $table = "midia_evento";
    protected $fillable = ['veiculo','imagem','linkNoticia'];
}
