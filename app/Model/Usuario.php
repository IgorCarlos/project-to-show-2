<?php

namespace App\Model;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;


class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuarios';
    protected $remember_token = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'senha'
    ];

    public function getAuthPassword() {
        return $this->senha;
    }

    public function usuarioComum() {
        return $this->hasOne('App\Model\UsuarioComum', 'usuarioId');
    }

    public function usuarioAdm() {
        return $this->hasOne('App\Model\UsuarioAdm', 'usuarioId');
    }

    // constantes
    const USUARIO_ATIVO = 1;
    const USUARIO_INATIVO = 0;


    // métodos auxiliares
    public function isInactive () {
        return $this->status == self::USUARIO_INATIVO;
    }

    public function isActive () {
        return $this->status == self::USUARIO_ATIVO;
    }

    public function statusPorEscrito () {

        switch ($this->status) {

            case self::USUARIO_ATIVO:
                return 'ativo';
                break;
            
            case self::USUARIO_INATIVO:
                return 'inativo';
                break;

            default:
                return 'não identificado';
                break;

        }

    }


    // CONTROLA STATUS
    public function inativa () {

        try {

            $this->status = self::USUARIO_INATIVO;
            $this->save();
    
            return true;

        } catch (\Exception $e) {

            return $e->getMessage();

        }

    }

    public function ativa () {
        
        try {

            $this->status = self::USUARIO_ATIVO;
            $this->save();
    
            return true;

        } catch (\Exception $e) {

            return $e->getMessage();

        }

    }

    public function statusToggle () {
        
        try {
            
            $this->status = $this->isActive() ? self::USUARIO_INATIVO : self::USUARIO_ATIVO;
            $this->save();

            return true;

        } catch (\Exception $e) {

            return $e->getMessage();

        }

    }

}
