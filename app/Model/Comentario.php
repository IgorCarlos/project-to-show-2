<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    public $fillable = [
        'usuarioComumId', 'artigoId', 'comentario', 'status', 'created_at', 'updated_at'
    ];

    public function usuarioComum () {
        return $this->hasOne('App\Model\UsuarioComum');
    }

    public function artigo () {
        return $this->hasOne('App\Model\Artigo');
    }

    const COMENTARIO_ATIVO = 1;
    const COMENTARIO_INATIVO = 0;

}
