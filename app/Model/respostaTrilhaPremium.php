<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class respostaTrilhaPremium extends Model
{
    protected $table = 'tp_respostas';
    public $primaryKey = 'id';
    public $fillable = ['etapa','desafioUp','termos_materiais','termos_relacionais','termos_mentais','termos_sociais','parar_fazer','continuar_fazendo','comecar_fazer','idUsuario','desafioUp1','desafioUp2','desafioUp3','desafioUp4'];

    public function usuarioComum(){
        return $this->hasOne('App/Model/UsuarioComum', 'idUsuario');
    }

}
