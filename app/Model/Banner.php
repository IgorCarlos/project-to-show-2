<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = "banners";

    protected $fillable = ['titulo', 'imagem', 'imagem_mobile', 'link', 'novaguia', 'status_banner'];

    protected $primaryKey = 'bannerId';

}
