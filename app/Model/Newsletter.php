<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table = 'newsletters';

    protected $fillable = [
        'name', 'email', 'phone', 'status', 'created_at', 'updated_at', 'isStudent'
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function writeStatus ($status) {
        switch ($status) {
            case self::STATUS_ACTIVE:
                return "Ativo";
                break;
            case self::STATUS_INACTIVE:
                return "Inativo";
                break;
        }
    }

}
