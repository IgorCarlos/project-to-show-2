<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ArtigoConteudo extends Model
{

    public $table = 'artigo_conteudo';
    protected $primaryKey = 'artigoId';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'artigoId', 'conteudo', 'multimidia'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 
    ];


    public function artigo () {
        return $this->belongsTo('App\Model\Artigo', 'artigoId');
    }

}
