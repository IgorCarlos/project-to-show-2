<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';
    public $primaryKey = 'id';
    public $fillable = [
        'nome, status, created_at, updated_at'
    ];

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public function artigo () {
        return $this->belongsToMany('App\Model\Artigo', 'artigos_tag', 'tagId', 'artigoId');
    }

    public function writeStatus ($status) {
        switch ($status) {
            case self::STATUS_ACTIVE:
                return "Ativo";
                break;
            case self::STATUS_INACTIVE:
                return "Inativo";
                break;
            default:
                return "Status não identificado";
                break;
        }
    }
}
