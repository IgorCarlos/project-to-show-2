<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Trilha extends Model
{
    protected $table = "trilha";

    protected $fillable = ['questionarioId','titulo','descricao', 'status','banner'];

//    public function conteudo(){
//        return $this->hasMany('App\Model\TrilhaConteudo', 'trilhaId');
//    }

    public function questionario () {
        return $this->belongsTo('App\Model\Questionario', 'questionarioId')->with('perguntas');
    }

}
