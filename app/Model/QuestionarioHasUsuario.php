<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestionarioHasUsuario extends Model
{
    protected $table = 'questionario_has_usuario';
    public $timestamps = false;

    public $fillable = [
        'id', 'usuarioComumId', 'questionarioId', 'created_at', 'resultado', 'respostas'
    ];

    public function usuario () {
        return $this->belongsTo('App\Model\UsuarioComum', 'usuarioId');
    }

    public function questionario () {
        return $this->belongsTo('App\Model\Questionario', 'questionarioId');
    }

}
