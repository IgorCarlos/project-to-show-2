<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FotoEvento extends Model
{
    protected $table = "fotos_eventos";
    protected $fillable = ['descricao', 'imagem'];
}
