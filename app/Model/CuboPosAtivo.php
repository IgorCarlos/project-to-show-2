<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CuboPosAtivo extends Model
{
    protected $table = 'tgt_dbm_cubo_pos_ativos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = [
        'DS_NOME_COMPLETO', 'NR_CPF', 'DS_EMAIL_TRAT', 'NR_CELULAR_COMPL_TRAT', 'DS_UNIDADE', 'DS_CURSO', 'DS_MODALIDADE'
    ];

    public function usuarioComum () {
        return $this->belongsTo('App\Model\UsuarioComum', 'NR_CPF', 'document');
    }

}
