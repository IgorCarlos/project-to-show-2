<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Artigo extends Model
{
    
    public $table = 'artigos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuarioAdmId', 'banner', 'titulo', 'status', 'conteudo_curto', 'conteudo', 'destaque', 'link', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 
    ];

    public function conteudo () {
        return $this->hasOne('App\Model\ArtigoConteudo', 'artigoId');
    }

    public function usuarioAdm () {
        return $this->belongsTo('App\Model\UsuarioAdm', 'usuarioAdmId')->with('usuario');
    }

    public function artigoTag () {
        return $this->hasMany('App\Model\ArtigoTag', 'artigoId');
    }

    public function tags () {
        return $this->belongsToMany('App\Model\Tag', 'artigos_tag', 'artigoId', 'tagId');
    }

    public function comentarios () {
        return $this->hasMany('App\Model\Comentario', 'artigoId');
    }


    const ARTIGO_ACTIVE = 1;
    const ARTIGO_INACTIVE = 0;
    const DESTAQUE_TRUE = 1;
    const DESTAQUE_FALSE = 0;


    public function writeAutor ($admId) {
        $usuarioAdm = UsuarioAdm::find($admId);
        return $usuarioAdm->usuario->nome;
    }

    public function writeDestaque ($destaque) {
        switch ($destaque) {
            case self::DESTAQUE_TRUE:
                return "Sim";
                break;

            case self::DESTAQUE_FALSE:
                return "Não";
                break;

            default:
                return "Não identificado";
        }
    }

    public function writeStatus ($status) {
        switch ($status) {
            case self::ARTIGO_ACTIVE:
                return "Ativo";
                break;

            case self::ARTIGO_INACTIVE:
                return "Inativo";
                break;

            default:
                return "Não identificado";
        }
    }

}
