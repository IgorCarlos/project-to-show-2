<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Questionario extends Model
{
    protected $table = "questionarios";
    
        protected $fillable = ['usuarioAdmId','status','titulo', 'descricao','resultadosId'];

        public function trilha () {
            return $this->hasOne('App\Model\Trilha', 'questionarioId');
        }

        public function perguntas (){
            return $this->hasMany('App\Model\QuestionarioPergunta', 'questionarioId');
        }

        public function usuarioAdm () {
            return $this->hasOne('App\Model\UsuarioAdm');
        }

        const STATUS_ACTIVE = 1;
        const STATUS_INACTIVE = 0;
}
