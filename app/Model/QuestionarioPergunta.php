<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestionarioPergunta extends Model
{
    protected $table = 'questionario_perguntas';
    public $timestamps = false;

    protected $fillable = [
        'questionarioId', 'descricao', 'status'
    ];
}
