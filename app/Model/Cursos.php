<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cursos extends Model
{
    use SoftDeletes;
    protected $table = "cursos";

    protected $fillable = ['nome'];

    public function pos()
    {
        return $this->belongsToMany('App\Model\Pos','curso_has_pos', 'id_curso', 'id_pos');
    }

    protected $dates = ['deleted_at'];

}
