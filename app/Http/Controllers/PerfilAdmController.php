<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\UsuarioAdm;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\UploadedFile;

class PerfilAdmController extends Controller
{

    public function viewEditar () {

        return view('app.perfil.editar')->with('administrador', Auth::user());
    }

    public function editar (Request $request) {
        
        $usuarioAdm = Auth::user();

        try {
            $usuarioAdm->email = $request->email;
            $usuarioAdm->name = $request->name;
    
            // upload foto adm
            if (!empty($request->file('imagem'))) {
                $imagem = $request->file('imagem')->store('public/fotoUsuarios');
                $usuarioAdm->imagem = $imagem;
            }
    
            $usuarioAdm->save();   

            flash('Dados atualizados com sucesso')->success();

        } catch (\Exception $e) {
            flash('Erro ao atualizar os dados')->error();
        }

        return redirect()->route('app::perfil::viewEditar');

    }

    public function atualizarSenha (Request $request) {
        
        $usuarioAdm = Auth::user();

        try {

            if (!Hash::check($request->senhaAtual, $usuarioAdm->senha)) throw new \Exception('Senha atual inválida', 400);

            $usuarioAdm->senha = Hash::make($request->senhaNova);
            $usuarioAdm->save();

            flash('Senha atualizada com sucesso')->success();

        } catch (\Exception $e) {
            flash($e->getMessage())->error();
        }

        return redirect()->route('app::perfil::viewEditar');

    }

}
