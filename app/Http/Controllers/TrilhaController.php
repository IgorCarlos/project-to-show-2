<?php

namespace App\Http\Controllers;

use App\Model\Questionario;
use App\Model\QuestionarioHasUsuario;
use App\Model\Usuario;
use Illuminate\Http\Request;
use App\Model\Trilha;
use App\Helper\Resposta;
use Validator;
use App\Model\TrilhaConteudo;

class TrilhaController extends Controller
{

    public function listar() {

        $data = [];
        $data['trilhas'] = Trilha::orderBy('id')->get();
        
        return view('app.trilhas.listar', $data);
    }

    public function postTrilha(Request $req) {
        $trilha = new Trilha();
        $input = $req->input();

        $validator = Validator::make($req->all(), [
            'titulo' => 'required|string',
            'descricao' => 'required',
        ],[
            'required' => 'O campo :attribute não foi preenchido',
        ]);


        $res = new Resposta();

        if($validator->fails()){
            $res->setErro(true);
            $res->setMensagem(['errors' => $validator->errors()]);
            return response($res->getResposta(), 400);
        }


        try{
            $trilha->create($input);
            $res->setMensagem("Produto cadastrado com sucesso");
            $res->setDados($trilha);
            return response($res->getResposta(), 200);
        }catch(\Exception $e){
            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), 400);
        }

    }

//    public function adicionaConteudo(Request $req) {
//        $trilha = Trilha::findOrFail($req->input('trilhaId'));
//        $res = new Resposta();
//        try{
//            $input = $req->input();
//            $input['conteudo'] = json_encode($input['secao']);
//
//            unset($input['secao']);
//            unset($input['trilhaId']);
//
////            $conteudo = $trilha->conteudo()->create($input);
//
//            $res->setMensagem("Conteudo adicionado com sucesso");
//            $res->setDados($conteudo);
//
//            return response($res->getResposta(), 200);
//        }
//        catch(Exception $e){
//            $res->setErro(true);
//            $res->setMensagem($e->getMessage());
//            return response($res->getResposta(), 400);
//        }
//
//    }

    public function getTrilha($id) {

        $trilha = Trilha::find($id);

//        $conteudo = $trilha->conteudo()->get();

//        foreach ($conteudo as $value){
////            $value->conteudo = json_decode($value->conteudo);
//
//            unset($value->created_at);
//            unset($value->updated_at);
//            unset($value->deleted_at);
//            unset($value->trilhaId);
//        }
        unset($trilha->questionarioId);
//        $trilha['conteudo'] = $conteudo;

        return $trilha;
    }

    public function editar($idTrilha) {

        $data['trilha'] = Trilha::where('id', $idTrilha)->get();
        $data['questionarios'] = Questionario::get();

        return view('app.trilhas.editar', $data);
    }

    public function edit($id, Request $req) {
        
        try{

            $trilha = Trilha::findOrFail($id);
            
            $hasImage = count($req->input());
            
            $input = $req->input();

            if($hasImage == 1) {
                $imagem = $req->file('banner')->store('public/trilhas');
                $input['banner'] = $imagem;
            }

            
            
            $trilha->update($input);

            flash('Trilha alterada com sucesso!')->success();
            
            return redirect(route('app::trilha::listar'));
        }catch(\Exception $e){
            flash('Erro ao cadastrar banner' . $e)->error();
            
            return redirect()->back()->withInput($req->input());
        }
        
    }


    // api
    public function apiListar (Request $request) {

        $res = new Resposta();

        try {

            $trilha = Trilha::with('questionario')
                ->where('id', $request->id)
                ->first();


            if (empty($trilha)) throw new \Exception("Nenhuma trilha foi encontrada", 400);

            $res->setDados($trilha);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {
            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());
        }

    }

    public function saveResult (Request $request) {

        $res = new Resposta();

        try {

            $validator = Validator::make($request->all(), [
                'usuarioId' => 'required|exists:usuarios_comum',
                'questionarioId' => 'required',
                'resultado' => 'required',
                'respostas' => 'required'
            ],[
                'required' => 'O campo :attribute não foi preenchido',
                'numeric' => 'O campo :attribute deve ser um número',
            ]);

            $res = new Resposta();

            if($validator->fails()){
                $res->setErro(true);
                $res->setMensagem(['errors' => $validator->errors()]);
                return response($res->getResposta(), 400);
            }

            $questionarioHasUsuario = QuestionarioHasUsuario::create([
                'usuarioComumId' => $request->usuarioId,
                'questionarioId' => $request->questionarioId,
                'resultado' => $request->resultado,
                'respostas' => $request->respostas,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $res->setMensagem("Resultado salvo com suceso");
            $res->setDados($questionarioHasUsuario);

            return response($res->getResposta(), 200);


        } catch (\Exception $e) {

            var_dump($e->getMessage());
            exit;

        }

    }

    public function cleanResult (Request $request) {

        $res = new Resposta();

        try {

            $validator = Validator::make($request->all(), [
                'usuarioId' => 'required|numeric',
                'questionarioId' => 'required|numeric'
            ],[
                'required' => 'O campo :attribute não foi preenchido',
                'numeric' => 'O campo :attribute deve ser do tipo numerico',
            ]);

            $res = new Resposta();

            if($validator->fails()){
                $res->setErro(true);
                $res->setMensagem(['errors' => $validator->errors()]);
                return response($res->getResposta(), 400);
            }

            $usuario = Usuario::with('usuarioComum')->find($request->usuarioId);

            if($request->questionarioId == 1){
                $user['tabVideo'] = 0;

                $usuario->usuarioComum()->update($user);
            }
            

            $questionarioHasUsuario = QuestionarioHasUsuario::where([
                'usuarioComumId' => $request->usuarioId,
                'questionarioId' => $request->questionarioId
            ])->delete();

            $res->setMensagem("Resultados apagados com sucesso");
            $res->setDados($questionarioHasUsuario);

            return response($res->getResposta(), 200);


        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());

            return response($res->getResposta(), $e->getCode());

        }

    }

}
