<?php

namespace App\Http\Controllers;

use App\Model\UsuarioComum;
use Illuminate\Http\Request;
use App\Model\Token;
use App\Model\TrilhaPremium;
use App\Helper\Resposta;
use App\Model\respostaTrilhaPremium;

class TrilhaPremiumController extends Controller
{
    public function addEtapa(Request $req, $etapa){

        $res = new Resposta();

        try{
            $hash = $req->header('Authorization');

            $usuario = Token::with('usuarioComum')->find($hash);

            $user = UsuarioComum::with('tp_resposta')->find($usuario->usuarioComum->usuarioId);

            $tp['etapa'] = $etapa;

            $user->tp_resposta()->update($tp);

            $res->setDados($user->tp_resposta);

            return Response($res->getResposta(), 200);
        }catch(\Exception $e){
            $res->setErro(true);

            return Response($res->getResposta(), 400);
        }
    }

    public function addEtapaPremium(Request $request){

        $res = new Resposta();

        try{
        
            $trilhaPremium = respostaTrilhaPremium::where('idUsuario', $request->usuarioId)->get();


            //$res->setDados();
            
            $res->setMensagem($trilhaPremium);
            return Response($res->getResposta(), 200);
        
        }catch(\Exception $e){
            
            $res->setErro(true);
            
            return Response($res->getResposta(), 400);
        }

    }

    public function addRespDesafioUp(Request $request){
        
        $trilhaPremium = respostaTrilhaPremium::where('idUsuario', $request->usuarioId)->first();

        $data['desafioUp1'] = $request->desafioUp1;
        $data['desafioUp2'] = $request->desafioUp2;
        $data['desafioUp3'] = $request->desafioUp3;
        $data['desafioUp4'] = $request->desafioUp4;

        $trilhaPremium->update($data);

        return $trilhaPremium;
    }

    public function artigos(Request $request){
        $trilhaPremium = respostaTrilhaPremium::where('idUsuario', $request->usuarioId)->first();
        
        $data['termos_materiais'] = $request->termos_materiais;
        $data['termos_relacionais'] = $request->termos_relacionais;
        $data['termos_mentais'] = $request->termos_mentais;
        $data['termos_sociais'] = $request->termos_sociais;

        $trilhaPremium->update($data);

        return $trilhaPremium;
    }

    public function pdi(Request $request){
        $trilhaPremium = respostaTrilhaPremium::where('idUsuario', $request->usuarioId)->first();
        
        $data['parar_fazer'] = $request->parar_fazer;
        $data['continuar_fazendo'] = $request->continuar_fazendo;
        $data['comecar_fazer'] = $request->comecar_fazer;

        $trilhaPremium->update($data);

        return $trilhaPremium;
    }
}
