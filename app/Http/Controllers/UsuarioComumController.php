<?php

namespace App\Http\Controllers;

use App\Mail\RequestRecoveryPassword;
use App\Model\QuestionarioHasUsuario;
use App\Model\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Helper\Resposta;
use App\Http\Requests\CadastraUsuarioComum;
use App\Http\Requests\AtualizaUsuarioComum;
use App\Model\UsuarioComum;
use App\Model\Usuario;
use Mail;
use Validator;


class UsuarioComumController extends Controller
{
    
    public function cadastra (Request $request) {

        $res = new Resposta();

        try {

            $validator = Validator::make($request->all(), [
//                'desafioAtual' => 'required',
                'document' => [
                    'required', 'unique:usuarios_comum', 'numeric',
                    //'regex:/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/'
                ],
                'name' => 'required|string',
                'dateOfBirth' => 'required|date',
                'email' => 'required|email|unique:usuarios',
                'phone' => 'required',
                'state' => 'required',
                'city' => 'required',
                'password' => 'required',
                'newsletter' => '',
//                'matriculado' => 'required|boolean',
                // 'created_at' => 'required|date'
//                'status' => 'numeric',
//                'sobrenome' => 'required|string',
            ], [
                'required' =>  ':attribute é um campo obrigatório.',
                'unique' => 'Este usuário já esta cadastrado',
                'numeric' => ':attribute deve ser um valor numérico',
                'max' => ':attribute excedeu a quantidade de caracter permitida',
                'boolean' => ':attribute aceita apenas valor como true ou false.',
                'string' => ':attribute deve conter apenas letras',
                'email' => ':attribute não esta em um formato correto',
                'date' => 'O campo data de nascimento deve ser uma data válida',
                'regex' => ':attribute não está no formato correto',
                'exists' => ':attribute id não é valido'
            ]);

            if ($validator->fails()) {
                $res->setMensagem($validator->errors());
                $res->setErro(true);
                return response($res->getResposta(), 400);
            }

            $body = $request->all();


            $body['status'] = Usuario::USUARIO_ATIVO;
            $body['password'] = Hash::make($request->password);

            $usuario = Usuario::create($body);
            $usuario->usuarioComum()->create($body);

            $tp['etapa'] = 1;

            $usuario->usuarioComum->tp_resposta()->create($tp);

            $res->setMensagem('Usuário criado');
            $res->setDados($usuario);

            return response($res->getResposta(), 201);

        } catch(\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            return response($res->getResposta(), 400);

        }

    }

    public function pegaUm (Request $request) {

        $res = new Resposta();

        try {
            
            if (!is_numeric($request->id)) throw new \Exception('Parâmetro ID deve ser um número', 400);

            $usuario = UsuarioComum::with(['usuario', 'cuboPos'])->where('usuarioId', $request->id)->first();

            $res->setDados($usuario);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());

        }

    }

    public function pegaTodos (Request $request) {

        $res = new Resposta();

        try {

            $usuario = UsuarioComum::with(['usuario', 'cuboPos'])->get()
;
            $res->setDados($usuario);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);
            return response($res->getResposta(), $e->getCode());

        }

    }

    public function statusToggle (Request $request) {

        $res = new Resposta();

        try {

            if (!is_numeric($request->id)) throw new \Exception('Parâmetro ID deve ser um número', 400);

            $usuario = UsuarioComum::with('usuario')->where('usuarioId', $request->id)->first();
            $usuario->usuario->status = $usuario->usuario->isActive() ? UsuarioComum::USUARIO_INATIVO : UsuarioComum::USUARIO_ATIVO;

            $usuario->usuario->save();

            $res->setMensagem('O status do usuário foi alterado para ' . $usuario->usuario->statusPorEscrito());

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            $status = $e->getCode() == 0 ? 500 : $e->getCode();

            return response($res->getResposta(), $status);

        }

    }

    public function atualiza (AtualizaUsuarioComum $request) {

        $res = new Resposta();
        
        try {

            if (empty($request->all())) {
                $res->setMensagem('Nenhuma alteração foi solicitada');
                return response($res->getResposta(), 200);
            }

            $usuarioComum = UsuarioComum::with('usuario')->where('usuarioId', $request->id)->first();
            $usuario = $usuarioComum->usuario;

            if (empty($usuarioComum)) throw new \Exception("Não conseguimos localizar sua conta de usuário, verifique se você está logado", 400);

            // verifica se o cpf informado já existe
            if ($usuarioComum->document !== $request->document) {
                $validaCPF = UsuarioComum::where('document', $usuarioComum->document)->first();
                if (!empty($validaCPF)) throw new \Exception("Este CPF já existe", 400);
            }

            // verifica se o novo email informado ja existe
            if ($usuario->email !== $request->email) {
                $validaEmail = Usuario::where('email', $request->email)->first();
                if (!empty($validaEmail)) throw new \Exception("Este e-mail já existe", 400);
            }


            $usuarioComum->update($request->all());
            $usuarioComum->usuario->update($request->all());

            $res->setMensagem('Seus dados foram atualizados com sucesso');
            $res->setDados($usuarioComum);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            return response($res->getResposta(), $e->getCode());

        }

    }

    public function ativa (Request $request) {
        
        $res = new Resposta();

        try {

            if (empty($request->id)) throw new \Exception('Necessário parâmetro ID', 400);

            $usuarioComum = UsuarioComum::find($request->id);

            if (empty($usuarioComum)) throw new \Exception('Nenhum usuáiro encontrado', 400);

            $usuarioComum->usuario->ativa();

            $res->setMensagem("Usuario {$usuarioComum->usuario->nome} ativo com sucesso");
            $res->setDados($usuarioComum);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());

            return response($res->getResposta(), $e->getCode());

        }

    }

    public function inativa (Request $request) {
        
        $res = new Resposta();

        try {

            if (empty($request->id)) throw new \Exception('Necessário parâmetro ID', 400);

            $usuarioComum = UsuarioComum::find($request->id);

            if (empty($usuarioComum)) throw new \Exception('Nenhum usuáiro encontrado', 400);

            $usuarioComum->usuario->inativa();

            $res->setMensagem("Usuario {$usuarioComum->usuario->nome} inativo com sucesso");
            $res->setDados($usuarioComum);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());

            return response($res->getResposta(), $e->getCode());

        }

    }

    public function getAnsweredFromTrail (Request $request) {

        $token = Token::where('hash', $request->header('Authorization'))
            ->with('usuarioComum')
            ->first();

        $trailAnswered = QuestionarioHasUsuario::where('usuarioComumId', $token->usuarioComum->usuarioId)
            ->with('questionario')
            ->get();

        $res = new Resposta();
        $res->setDados($trailAnswered);

        return response($res->getResposta(), 200);

    }


    public function recoveryPassword (Request $request) {

        $res = new Resposta();

        try {

            if (empty($request->recoveryEmail)) throw new \Exception("e-mail é obrigatório para recuperar sua senha", 400);

            $usuarioComum = UsuarioComum::whereHas('usuario', function ($query) use ($request) {
                $query->where('email', $request->recoveryEmail);
            })
                ->with('usuario', 'token')
                ->first();

            if (empty($usuarioComum)) throw new \Exception("Nenhum usuário encontrado com este e-mail", 400);


            Mail::to($usuarioComum->usuario->email)->send(new RequestRecoveryPassword($usuarioComum));

            $res->setMensagem("E-mail de recuperação enviado com sucesso");

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {
            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());
        }

    }

    public function newPassword (Request $request) {

        $res = new Resposta();

        try {

            if (empty($request->hash)) throw new \Exception("Não identificamos o seu código para alteração da senha", 400);
            if (empty($request->newpassword)) throw new \Exception("Nova senha não localizada", 400);

            $usuarioComum = UsuarioComum::whereHas('token', function ($query) use ($request) {
                $query->where('hash', $request->hash);
            })
                ->with('usuario')
                ->first();

            if (empty($usuarioComum)) throw new \Exception("Código inválido", 400);

            $usuarioComum->usuario->password = Hash::make($request->newpassword);
            $usuarioComum->usuario->save();

            $res->setMensagem("Senha alterada com sucesso");
            $res->setDados($usuarioComum);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {
            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());
        }


    }

}
