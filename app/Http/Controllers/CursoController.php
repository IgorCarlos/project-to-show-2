<?php

namespace App\Http\Controllers;

use App\Helper\Resposta;
use Validator;
use Illuminate\Http\Request;
use App\Model\Cursos;
use App\Model\Polo;
use Illuminate\Support\Facades\Storage;
use App\Model\Pos;


class CursoController extends Controller
{
    public function viewAddCurso(){

        return view('app.pos.novo');
    }


    public function listar(){

        $dados['cursos'] = Cursos::paginate(10);
        $dados['cursospos'] = Pos::paginate(10);

        return view('app.pos.lista', $dados);
    }

    public function addCurso(Request $req){

        $validator = Validator::make($req->all(), [
            'nome' => 'required|unique:cursos',
        ],[
            'required' => 'O campo :attribute não foi preenchido',
            'unique' => 'Este curso já esta cadastrado'
        ]);


        if($validator->fails()){
            return redirect(route('app::curso::cadastrar'))->withErrors($validator)->withInput();
        }

        try{
            $curso = new Cursos();

            $curso->create($req->input());

            flash('curso cadastrado com sucesso')->success();
            return redirect(route('app::curso::cadastrar'));
        }catch(\Exception $e){

            flash('erro ao cadastrar curso')->error();

            return redirect(route('app::curso::cadastrar'));

        }

    }

    public function viewCadastrarPos(){
        return view('app.pos.novopos');
    }

    public function CadastrarPos(Request $req){
        $validator = Validator::make($req->all(), [
            'nome' => 'required|unique:cursos',
        ],[
            'required' => 'O campo :attribute não foi preenchido',
            'unique' => 'Este curso já esta cadastrado'
        ]);


        if($validator->fails()){
            return redirect(route('app::curso::cadastrar'))->withErrors($validator)->withInput();
        }

        try{
            $curso = new Pos();

            $curso->create($req->input());

            flash('Pós cadastrado com sucesso')->success();
            return redirect(route('app::curso::cadastrar'));
        }catch(\Exception $e){

            flash('erro ao cadastrar curso')->error();

            return redirect(route('app::curso::cadastrar'));

        }
    }

    public function listarpos(){
        $data['cursos'] = Cursos::paginate(10);

        return view('app.pos.listapos', $data);
    }

    public function vercurso($id){
        $data['curso'] = Cursos::with('pos')->find($id);

        $data['pos'] = Pos::get();

        $data['ArrPos'] = [];
        foreach($data['curso']->pos as $cPos){
             array_push($data['ArrPos'], $cPos->id);
        }

       return view('app.pos.vercurso', $data);
    }

    public function adicionaPosAoCurso($idCurso, Request $req){
        $curso = Cursos::find($idCurso);

        $cursosPos = $req->input('pos');

        foreach($cursosPos as $cPos){
            $curso->pos()->attach($cPos);
        }


        flash("Pos vinculada com sucesso")->success();
        return redirect(route('app::curso::vercurso', ['id' => $idCurso]));
    }

    public function detachPos($idCurso, $idPos){

        try{
            $curso = Cursos::find($idCurso);
            $curso->pos()->detach($idPos);

            flash("Pos desvinculada com sucesso")->success();
            return redirect(route('app::curso::vercurso', ['id' => $idCurso]));
        }catch (\Exception $e){

        }

    }

    public function softDelete($idCurso){

        try{
            $curso = Cursos::find($idCurso);
//            $curso->nome = "deleted";
            $curso->delete();

            flash("Curso deletado com sucesso")->success();
            return redirect(route('app::curso::viewListar'));
        }catch(\Exception $e){

            return redirect(route('app::curso::viewListar'));
        }

    }

    public function listenUser (Request $request) {

        $res = new Resposta();

        try {

            $cursos = Cursos::where('nome', 'like', "%$request->reference%")
                ->select('nome', 'id')
                ->get();

            if (empty($cursos)) throw new \Exception("Nenhum curso encontrado", 200);

            $res->setDados($cursos);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {
            $res->setErro(true);
            $res->setMensagem('Nenhum curso localizado');

            return response($res->getResposta(), $e->getCode());
        }

    }

    public function getByCourse($course){    
        $curso = Cursos::where('nome', 'like', '%' . $course . '%')->with('pos')->first();
        $polo = Polo::get();
        
        $curso['instituicoes'] = $polo;

        foreach($polo as $p){
            
            $url = url('/').Storage::url($p->logo);
            
            $p->logo = $url;
        
        }
    
        return $curso;
    }
}
