<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Helper\Resposta;
use App\Http\Requests\CadastraUsuarioAdm;
use App\Http\Requests\AtualizaUsuarioComum;
use App\Model\UsuarioAdm;
use App\Model\Usuario;
use Validator;

class UsuarioAdmController extends Controller
{
    
    public function cadastra (Request $request) {

        $res = new Resposta();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
//            'sobrenome' => 'required|string',
            'email' => 'required|email|unique:usuarios',
            'password' => 'required',
//            'status' => 'numeric'
        ],[
            'required' => 'O campo :attribute não foi preenchido',
            'string' => 'O campo :attribute deve ser do tipo string',
            'email' => 'O campo :attribute deve conter um formato de email',
            'numeric' => 'O campo :attribute deve ser do tipo numérico',
            'unique' => 'Esté usuário ja foi cadastrado'
        ]);

        if ($validator->fails()) {

            $res->setErro(true);
            $res->setMensagem(['errors' => $validator->errors()]);
            return response($res->getResposta(), 400);

        }

        
        $body = $request->all();
        $body['status'] = Usuario::USUARIO_ATIVO;
        $body['password'] = Hash::make($request->senha);

        try {

            $usuario = Usuario::create($body);
            $usuario->usuarioAdm()->create($body);

            $res->setErro(false);
            $res->setMensagem('Usuário criado');
            $res->setDados($usuario);

            return response($res->getResposta(), 201);

        } catch(\Exception $e) {

            $res->setResposta(true, $e->getMessage(), []);

            return response($res->getResposta(), 400);

        }

    }

    public function pegaUm (Request $request) {

        $res = new Resposta();

        try {
            
            if (!is_numeric($request->id)) throw new \Exception('Parâmetro ID deve ser um número', 400);

            $usuario = UsuarioAdm::with('usuario')->where('usuarioId', $request->id)->first();

            $res->setDados($usuario);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());

        }

    }

    public function pegaTodos (Request $request) {

        $res = new Resposta();

        try {

            $usuario = UsuarioAdm::with('usuario')->get();

            $res->setDados($usuario);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);
            return response($res->getResposta(), $e->getCode());

        }

    }

    public function atualiza (AtualizaUsuarioComum $request) {

        $res = new Resposta();
        
        try {

            if (empty($request->all())) {
                $res->setMensagem('Nenhuma alteração foi solicitada');
                return response($res->getResposta(), 200);
            }

            // memoria dos dados
            $body = $request->all();

            // encripta a nova senha caso houver
            if (!empty($body['senha'])) $body['senha'] = Hash::make($body['senha']);

            // realiza o update aqui
            $usuarioAdm = UsuarioAdm::with('usuario')->where('usuarioId', $request->id)->first();
            $usuarioAdm->update($request->all());
            $usuarioAdm->usuario->update($request->all());

            $res->setMensagem('Dados do usuario ' . $usuarioAdm->usuario->name . ' foram atualizados com sucesso');
            $res->setDados($usuarioAdm);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            return respose($res->getResposta(), $e->getCode());

        }

    }

    public function ativa (Request $request) {
        
        $res = new Resposta();

        try {

            if (empty($request->id)) throw new \Exception('Necessário parâmetro ID', 400);

            $usuarioAdm = UsuarioAdm::find($request->id);

            if (empty($usuarioAdm)) throw new \Exception('Nenhum usuáiro encontrado', 400);

            $usuarioAdm->usuario->ativa();

            $res->setMensagem("Usuario {$usuarioAdm->usuario->nome} ativo com sucesso");
            $res->setDados($usuarioAdm);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());

            return response($res->getResposta(), $e->getCode());

        }

    }



}
