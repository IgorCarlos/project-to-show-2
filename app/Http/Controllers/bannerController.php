<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Banner;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Helper\Resposta;


class bannerController extends Controller
{


    public function getBanners(){
        $banner = new Banner();

        $banners = $banner->paginate(1);

        foreach ($banners as $b){
            $url = Storage::url($b->imagem);

            $b->imagem = $url;
        }

        $data['banners'] = $banners;

        return view('app.banner.lista', $data);
    }



    public function novo(Request $req){
        if($req->isMethod('post')){
            $validator = Validator::make($req->all(), [
                'titulo' => 'required',
                'imagem' => 'required|image|between:0,2000',
                'imagemMobile' => 'required'
            ],[
                'required' => 'O campo :attribute não foi preenchido',
                'dimensions' => 'A imagem está fora das dimensões',
                'image' => 'O arquivo não é imagem',
                'between' => 'A imagem ultrapassou o limite de tamanho'
            ]);

            if($validator->fails()){
                return redirect(route('app::banner::novo'))->withErrors($validator)->withInput();
            }


            try{
                $banner = $req->input();
                unset($banner['_token']);
                
                $banner['novaguia'] = (isset($banner['novaguia'])) ? true : false;
                $banner['status_banner'] = 1;

                $imagem = $req->file('imagem')->store('public/banners');
                $banner['imagem'] = $imagem;

                
                $imagemMobile = $req->file('imagemMobile')->store('public/banners/mobile');
                $banner['imagem_mobile'] = $imagemMobile;


                Banner::create($banner);

                flash('Banner cadastrado com sucesso!')->success();

                return redirect(route('app::banner::novo'));

            }catch(\Exception $e){
                flash('Erro ao cadastrar banner' . $e)->error();

                return redirect()->back()->withInput($req->input());
            }

        }else if($req->isMethod('get')){

            return view('app.banner.novo');
        }
    }



    public function changeStatus(Request $req){
        $banner = new Banner();
        $b = $banner->findOrFail($req->id);

        if($req->status == 0){
            $b['status_banner'] = 1;
        }else{
            $b['status_banner'] = 0;
        }

        $b->save();

        print_r($b);

        flash('Banner alterado com sucesso')->success();

        return redirect(route('app::banner::getBanners'));
    }


    public function deletar(Request $req){

        $banner = new Banner();

        $b = $banner->findOrFail($req->id);

        $b->delete();

        flash('Banner deletado com sucesso')->success();

        return redirect(route('app::banner::getBanners'));
    }


    public function todos(){

        $res = new Resposta();


        try{
            $banner = new Banner();
            $banners = $banner->where('status_banner', 1)->get();



            foreach ($banners as $banner){


                $banner['src'] = url('/').Storage::url($banner['imagem']);
                $banner['src_mobile'] = url('/').Storage::url($banner['imagem_mobile']);
                $banner['blank'] = ($banner['novaguia'] == 1) ? true : false;
                $banner['title'] = $banner['titulo'];


                unset($banner['imagem']);
                unset($banner['status_banner']);
                unset($banner['created_at']);
                unset($banner['updated_at']);
                unset($banner['titulo']);
                unset($banner['novaguia']);
                unset($banner['mostratd']);
                unset($banner['imagem_mobile']);


            }

            $res->setDados($banners);

            return response($res->getResposta(), 200);

        }catch(\Exception $e){
            $res->setMensagem($e->getMessage());

            $res->setErro(true);

            return response($res->getResposta(), $e->getCode());
        }

        return $banners;
    }

}
