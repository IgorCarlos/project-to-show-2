<?php

namespace App\Http\Controllers;

use App\Helper\Resposta;
use App\Model\Tag;
use Validator;
use Illuminate\Http\Request;

class TagsController extends Controller
{

    public function viewCadastra () {
        return view('app.tags.cadastra');
    }

    public function viewListar () {

        return view('app.tags.listar', ['tags' => Tag::get()]);
    }

    //
    public function cadastra (Request $request) {

        $validator = Validator::make($request->all(), [
            'nome' => 'required'
        ], [
            'required' => 'O campo :attribute é obrigatório'
        ]);

        if ($validator->fails()) {
            flash("Erro de validação")->error();
            return redirect(route('app::tags::viewCadastra'));
        }

        $tag = new Tag();
        $tag->nome = $request->nome;
        $tag->status = Tag::STATUS_ACTIVE;

        $tag->save();

        flash("Tag {$tag->nome} cadastrada com sucesso")->success();

        return redirect(route('app::tags::viewCadastra'));

    }

    public function inativar (Request $request) {

        if (empty($request->id)) {
            flash('Falha ao trocar o status da tag');
            return redirect(route('app::tags::viewLista'));
        }

        $tag = Tag::find($request->id);
        $tag->status = Tag::STATUS_INACTIVE;
        $tag->save();

        flash("A tag {$tag->nome} foi inativada com sucesso");

        return redirect(route('app::tags::viewLista'));
    }

    public function ativar (Request $request) {

        if (empty($request->id)) {
            flash('Falha ao trocar o status da tag');
            return redirect(route('app::tags::viewLista'));
        }

        $tag = Tag::find($request->id);
        $tag->status = Tag::STATUS_ACTIVE;
        $tag->save();

        flash("A tag {$tag->nome} foi ativada com sucesso");

        return redirect(route('app::tags::viewLista'));
    }

}
