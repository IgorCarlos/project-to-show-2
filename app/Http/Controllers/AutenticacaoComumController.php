<?php

namespace App\Http\Controllers;

use App\Model\Usuario;
use Illuminate\Http\Request;
use App\Model\Token;
use App\Model\UsuarioComum;
use App\Model\QuestionarioHasUsuario;
use App\Http\Requests\AutenticacaoComum;
use Validator;
use App\Helper\Resposta;
use Hash;

class AutenticacaoComumController extends Controller
{

    public function autentica (Request $request) {

        $res = new Resposta();

        try {


            // request validation
            $validator = Validator::make($request->all(), [
                'email' => 'required|exists:usuarios,email',
                'password' => 'required'
            ], [
                'exists' => ':attribute não encontrado',
                'required' => ':attribute é um campo obrigatório'
            ]);

            // valida se existe algum erro de dados da requisição
            if ($validator->fails()) throw new \Exception($validator->errors(), 400);

            // pega o usuário com os dados
            $usuario = Usuario::with('usuarioComum')->where('email', $request->email)->first();


            // valida se as credenciais do usuário existe
            if (empty($usuario)) throw new \Exception('O usuário não foi encontrado', 400);

            $usuarioComum = $usuario->usuarioComum;

            // valida se a senha corresponde
            if (!Hash::check($request->password, $usuario->password)) throw new \Exception('Credenciais inválidas', 403);


            // validação do token
            if (empty($usuarioComum->token)) {
                $usuarioComum->token()->create(Token::generate());
            } else {
                $usuarioComum->token->renew();
            }

            $retorno = Usuario::with('usuarioComum')->where('email', $request->email)->first();

            // no retorno foi necessário rebuscar o usuário para capturar o token atrelado
            $res->setDados([
                'token' => Usuario::with('usuarioComum')->where('email', $request->email)->first()->usuarioComum->token->hash,
                'user' => $retorno
            ]);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {
            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            if($e->getCode() == 403){
                $code = 403;
            }else{
                $code = 400;
            }
            return response($res->getResposta(), $code);
        }

    }

    public function autenticaWithFB (Request $request) {
        
        $res = new Resposta();

        try {
        
            if (empty($request->email)) throw new \Exception("E-mail não informado", 400);

            $usuario = Usuario::where('email', $request->email)
                ->with('usuarioComum')
                ->first();

            // caso não houver nenhum cadastro com a conta do facebook
            if (empty($usuario)) {
    //                $body = $request->all();
    //                $body['status'] = Usuario::USUARIO_ATIVO;
    //                $usuario = Usuario::create($body);
    //                $usuario->usuarioComum()->create($body);
                $res->setMensagem("Complete seu cadastro");
                $res->setDados([
                    'continue_with_facebook' => true
                ]);
                $res->setErro(true);
                return response($res->getResposta(), 200);
            }

            $usuarioComum = $usuario->usuarioComum;

            // validação do token
            if (empty($usuarioComum->token)) {
                $usuarioComum->token()->create(Token::generate());
            } else {
                $usuarioComum->token->renew();
            }

            $res->setMensagem("Autenticação com facebook concluida");
            $res->setDados($usuario);

            $retorno = Usuario::with('usuarioComum')->where('email', $request->email)->first();

            // no retorno foi necessário rebuscar o usuário para capturar o token atrelado
            $res->setDados([
                'token' => Usuario::with('usuarioComum')->where('email', $request->email)->first()->usuarioComum->token->hash,
                'user' => $retorno
            ]);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {
            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());
        }
        
        
    }

    public function getData(Request $req){

        $res = new Resposta();

        try{
            $hash = $req->header('Authorization');
            
            $usuario = Token::with('usuarioComum')->find($hash);

            $dados = UsuarioComum::with('tp_resposta', 'usuario', 'cuboPos')->find($usuario->usuarioComumId);

            $trilheSucesso = QuestionarioHasUsuario::orderBy('id', 'DESC')->where('questionarioId', '1')->where('usuarioComumId', $dados->usuario->id)->first();

            $descubraPerfil = QuestionarioHasUsuario::orderBy('id', 'DESC')->where('questionarioId', '2')->where('usuarioComumId', $dados->usuario->id)->first()
            ;
            $trilhaPremium = QuestionarioHasUsuario::orderBy('id', 'DESC')->where('questionarioId', '3')->where('usuarioComumId', $dados->usuario->id)->first();

            $descubra['respondido'] = false;

            $trilhe['respondido'] = false;

            $premium['respondido'] = false;

            if(count($trilheSucesso) >= 1){
                $trilhe['resultado'] = json_decode($trilheSucesso->resultado);
                $trilhe['respostas'] = json_decode($trilheSucesso->respostas);
                $trilhe['respondido'] = true;
            }

            if(count($descubraPerfil)){
                $descubra['resultado'] = json_decode($descubraPerfil->resultado);
                $descubra['respostas'] = json_decode($descubraPerfil->respostas);
                $descubra['respondido'] = true;
            }

            if(count($trilhaPremium)){
                $premium['resultado'] = json_decode($trilhaPremium->resultado);
                $premium['respostas'] = json_decode($trilhaPremium->respostas);
                $premium['respondido'] = true;
            }

            $user['id'] = $dados->usuario->id;
            $user['name'] = $dados->usuario->name;
            $user['email'] = $dados->usuario->email;
            $user['imagem'] = $dados->usuario->imagem;
            $user['isStudent'] = $dados->usuarioComum->matriculado;
            $user['tp_resposta'] = $dados->usuarioComum->tp_resposta;
            $user['dateOfBirth'] = $dados->usuarioComum->dateOfBirth;
            $user['state'] = $dados->usuarioComum->state;
            $user['city'] = $dados->usuarioComum->city;
            $user['phone'] = $dados->usuarioComum->phone;
            $user['document'] = $dados->usuarioComum->document;
            $user['registration'] = $dados->usuarioComum->registration;
            $user['logged'] = true;
            $user['id'] = $dados->usuario->id;
            $user['video'] = $dados->usuarioComum->tabVideo;
            $user['track_success'] = $trilhe;
            $user['discover_profile'] = $descubra;
            $user['cubo_pos'] = $dados->usuarioComum->cuboPos;
            $user['has_cubo_pos'] = !empty($dados->usuarioComum->cuboPos) ? true : false;
            $user['trilha_premium'] = $premium;

            $res->setDados($user);

            return Response($res->getResposta(), 200);
        }catch(\Exception $e){

            $res->setErro(true);
            $res->setMensagem($e->getMessage());

            return Response($res->getResposta(), 400);
        }


    }


    public function watched(Request $req, $tab){

        $res = new Resposta();


        try{

            $hash = $req->header('authorization');
            $usuario = Token::with('usuarioComum')->find($hash);
            $data['tabVideo'] = $tab;

            $usuario->usuarioComum->update($data);


            $res->setMensagem("atualizado com sucesso");
            
            return Response($res->getResposta(), 200);

        }catch(\Exception $e){
            return $e->getMessage();
        }

    }
}
