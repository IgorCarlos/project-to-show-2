<?php

namespace App\Http\Controllers;

use App\Model\Evento;
use App\Model\Unidade;
use Illuminate\Http\Request;
use App\Model\Inscritos;
use Excel;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Midia;
use App\Model\FotoEvento;
use Illuminate\Support\Facades\Storage;
class EventoController extends Controller
{
    public function index(){
        $eventos = Evento::with('unidades','inscritos')->get();

        $data['eventos'] = $eventos;
        return view('app.evento.index', $data);
    }

    public function viewNovo(){

        return view('app.evento.novo');
    }

    public function novo(Request $req){
            $objEvento = new Evento();

            $input = $req->input();


            $evento['datainicio'] = $this->converteData(date('d/m/Y', strtotime($input['data1'])));
            $evento['datafim'] = $this->converteData(date('d/m/Y', strtotime($input['data2'])));
            $evento['mes'] = $this->retornaMes(date('d/m/Y', strtotime($input['data1'])));


            $evento['descricao'] = $input['titulo'];
            $evento['linkvideo'] = $input['linkvideo'];
            $evento['descricaopalestrante'] = $input['descricaopalestrante'];
            $evento['nomepalestrante'] = $input['nomepalestrante'];


            $objEvento->create($evento);

            flash("Evento criado com sucesso")->success();

            return redirect(route('app::evento::inicio'));


    }

    public function converteData($data){
        $dates = explode('/', $data);
        $dateFormated = $dates[2]."-".$dates[1]."-".$dates[0];

        return str_replace(' ', '',$dateFormated);
    }

    public function retornaMes($inicio){
        $dates = explode('/', $inicio);

        $mes = $dates[1];

        switch ($mes){
            case 1: $mesEscrito = "Janeiro";
            break;
            case 2: $mesEscrito = "Fevereiro";
            break;
            case 3: $mesEscrito = "Março";
            break;
            case 4: $mesEscrito = "Abril";
            break;
            case 5: $mesEscrito = "Maio";
            break;
            case 6: $mesEscrito = "Junho";
            break;
            case 7: $mesEscrito = "Julho";
            break;
            case 8: $mesEscrito = "Agosto";
            break;
            case 9: $mesEscrito = "Setembro";
            break;
            case 10: $mesEscrito = "Outubro";
            break;
            case 11: $mesEscrito = "Novembro";
            break;
            case 12: $mesEscrito = "Dezembro";
            break;
            default: $mesEscrito = "Janeiro";
            break;
        }

        return $mesEscrito;
    }

    public function getAPI(){
        $evento = Evento::where('status', 1)->first();

        $data['de'] = date('d', strtotime($evento->datainicio));
        $data['ate'] = date('d', strtotime($evento->datafim));
        $data['palestrante'] = $evento->nomepalestrante;
        $data['descricao'] = $evento->descricaopalestrante;
        $data['video'] = $evento->linkvideo;
        $data['mes'] = $evento->mes;
        return $data;

    }

    public function verunidades($id){
        $unidades = Unidade::with('polo')->get();

        $data['idEvento'] = $id;
        $eventos = Evento::with('unidades')->find($id);

        foreach($unidades as $unidade){

            foreach($eventos->unidades as $unitEvt){

                if($unidade->id == $unitEvt->id){

                    $unidade->dias = explode(',', $unitEvt->pivot->dia_evento);
                }
            }


        }

        $data['unidades'] = $unidades;

        $evento = Evento::where('status',1)->first();

        $data['de'] = date('d', strtotime($evento->datainicio));
        $data['ate'] = date('d', strtotime($evento->datafim));

        //$data['dias'] = $ate - $de;

        $data['evento'] = $evento;


        return view('app.evento.unidades', $data);
    }

    public function salvarDatas(Request $req, $id){


        $evento = Evento::find($id);
        $unidades = $req->input('unidade');
        $toExclude = DB::table('evento_has_unidade')->where('eventoId', $id)->delete();


        if(count($unidades) > 0){
            foreach($unidades as $key => $value){

                $variavelTeste = implode($value, ',');
                $evt['dia_evento'] = $variavelTeste;
                $evt['hora_evento'] = 19;
                $evt['unidadeId'] = $key;

                $evento->unidades()->attach($evt['unidadeId'],['dia_evento' => $evt['dia_evento'], 'hora_evento' => $evt['hora_evento']]);

            }

            flash("datas alteradas com sucesso")->success();
        }



        return redirect(route('app::evento::unidades', ['id' => $id]));
    }

    public function verInscritos($id){
        $data['evento'] = $id;
        $data['inscritos'] = Inscritos::with('unidades')->where('idEvento', $id)->get();


        return view('app.evento.inscritos', $data);
    }

    public function confirmarInscrito($userId){
        $user = Inscritos::find($userId);

        $user->confirmado = true;

        $user->save();

        return redirect(route('app::evento::inscritos', ['id' => $user->idEvento]));

    }

    public function gerarXls($id){
        $inscritos = Inscritos::with('unidades')->where('idEvento', $id)->get();

        Excel::create('matriculas', function($excel) use($inscritos){

            $excel->sheet('matriculas', function($sheet) use($inscritos) {

                $sheet->appendRow(array(
                    'ID', 'DATA','NOME','E-MAIL','TELEFONE','CPF','DIA','UNIDADE','INFO','CONFIRMADO'
                ));
                foreach($inscritos as $inscrito){

                    $sheet->appendRow(array(
                        $inscrito->id,
                        date('d/m/Y', strtotime($inscrito->created_at)),
                        $inscrito->nome,
                        $inscrito->email,
                        $inscrito->telefone,
                        $inscrito->documento,
                        date('d', strtotime($inscrito->dia)),
                        $inscrito->unidades->cidade . '/' . $inscrito->unidades->estado,
                        $inscrito->unidades->endereco,
                        ($inscrito->confirmado) ? 'sim' : 'não'
                    ));
                }

            });

        })->download('xls');


        return redirect(route('app::evento::inscritos', ['id' => $id]));
    }

    public function verMidias(){
        $data['midias'] = Midia::get();

        return view('app.evento.listarMidia', $data);
    }

    public function novaMidia(){
        return view('app.evento.novamidia');
    }

    public function addMidia(Request $req){

        $validator = Validator::make($req->all(), [
            'titulo' => 'required',
            'imagem' => 'required|image|between:0,2000',
            'link' => 'required'
        ],[
            'required' => 'O campo :attribute não foi preenchido',
            'dimensions' => 'A imagem está fora das dimensões',
            'image' => 'O arquivo não é imagem',
            'between' => 'A imagem ultrapassou o limite de tamanho'
        ]);

        if($validator->fails()){
            return redirect(route('app::evento::novaMidia'))->withErrors($validator)->withInput();
        }

        try{
            $midia = $req->input();

            $imagem = $req->file('imagem')->store('public/midias');
            $midia['imagem'] = $imagem;
            $midia['veiculo'] = $midia['titulo'];
            $midia['linkNoticia'] = $midia['link'];

            Midia::create($midia);

            flash('Noticia cadastrado com sucesso!')->success();

            return redirect(route('app::evento::novaMidia'));

        }catch(\Exception $e){
            flash('Erro ao cadastrar noticia' . $e)->error();

            return redirect()->back()->withInput($req->input());
        }

    }

    public function excluirMidia($id){
        $midia = Midia::find($id);
        $midia->delete();

        return redirect(route('app::evento::midias'));
    }

    public function verFotos(){
        $data['fotos'] = FotoEvento::get();

        foreach ($data['fotos'] as $f){
            $url = Storage::url($f->imagem);

            $f->imagem = $url;
        }


        return view('app.evento.listafoto', $data);
    }

    public function addFotos(Request $req){

        $validator = Validator::make($req->all(), [
            'descricao' => 'required',
            'imagem' => 'required|image|between:0,2000',
        ],[
            'required' => 'O campo :attribute não foi preenchido',
            'dimensions' => 'A imagem está fora das dimensões',
            'image' => 'O arquivo não é imagem',
            'between' => 'A imagem ultrapassou o limite de tamanho'
        ]);

        if($validator->fails()){
            return redirect(route('app::evento::fotos'))->withErrors($validator)->withInput();
        }

        try{

            $foto = $req->input();

            $imagem = $req->file('imagem')->store('public/eventos');
            $foto['imagem'] = $imagem;

            FotoEvento::create($foto);

            flash('Foto cadastrado com sucesso!')->success();

            return redirect(route('app::evento::fotos'));

        }catch(\Exception $e){
            flash('Erro ao cadastrar foto' . $e)->error();

            return redirect()->back()->withInput($req->input());
        }
    }
}
