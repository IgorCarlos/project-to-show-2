<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Resposta;
use App\Http\Requests\CadastraComentario;
use Validator;
use App\Model\Comentario;
use App\Model\Artigo;

class ComentarioController extends Controller
{

    public function cadastra (Request $request) {

        $res = new Resposta();

        $validator = Validator::make($request->all(), [
            'artigoId' => 'required|numeric|exists:artigos,id',
            'usuarioComumId' => 'required|numeric|exists:usuarios_comum,usuarioId',
            'comentario' => 'required'
        ], [
            'required' => 'O :attribute é um campo requerido',
            'numeric' => 'O :attribute deve ser do tipo numérico',
            'exists' => 'O :attribute não foi encontrado'
        ]);

        if ($validator->fails()) {

            $res->setErro(true);
            $res->setMensagem($validator->errors());

            return response($res->getResposta(), 400);
        }

        //
        $body = $request->all();
        $body['status'] = Comentario::COMENTARIO_ATIVO;

        //
        $comentario = Comentario::create($body);

        $res->setDados($comentario);
        $res->setMensagem('Comentário cadastrado com sucesso');

        return response($res->getResposta(), 200);

    }

}
