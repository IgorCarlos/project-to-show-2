<?php

namespace App\Http\Controllers;

use App\Helper\Resposta;
use App\Model\Artigo;
use App\Model\Tag;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;

class BuscaController extends Controller
{

    public function busca (Request $request) {

        $artigosEncontrados = Artigo::where('titulo', 'like', "%$request->reference%")
            ->orWhere('conteudo_curto', 'like', "%$request->reference%")
            ->orWhere('conteudo', 'like', "%$request->reference%")
            ->get();

        $tagsEncontradas = Tag::where('nome', 'like', "%$request->reference%")
            ->with('artigo')
            ->get();


        $artigosResponse = [];

        if (!empty($artigosEncontrados)) {

            if (!empty($tagsEncontradas)) {

                $nomesArtigosEncontrados = [];
                foreach ($artigosEncontrados as $artigosEncontrado) {
                    $artigosEncontrado['capa'] =  url('/').Storage::url($artigosEncontrado['banner']);
                    $nomesArtigosEncontrados[] = $artigosEncontrado->titulo;
                }

                $filtroArtigosEncontradosNaTag = [];
                foreach ($tagsEncontradas as $tagsEncontrada) {
                    foreach ($tagsEncontrada->artigo as $artigo) {
                        if (!in_array($artigo->titulo, $nomesArtigosEncontrados)) {
                            $filtroArtigosEncontradosNaTag[] = $artigo;
                        }
                    }
                }

                $artigosResponse = array_merge($artigosEncontrados->toArray(), $filtroArtigosEncontradosNaTag);

            } else {

                $artigosResponse = $artigosEncontrados;

            }

        } else if (!empty($tagsEncontradas)) {

            foreach ($tagsEncontradas as $tagsEncontrada) {
                foreach ($tagsEncontrada['artigo'] as $artigo) {
                    $artigo['capa'] =  url('/').Storage::url($artigo['banner']);
                    $artigosResponse[] = $artigo;
                }
            }

        }


        $res = new Resposta();
        $res->setDados($artigosResponse);

        return response($res->getResposta(), 200);

    }

}
