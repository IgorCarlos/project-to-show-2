<?php

namespace App\Http\Controllers;

use App\Model\QuestionarioPergunta;
use Illuminate\Http\Request;
use App\Model\Questionario;
use Illuminate\Support\Facades\Auth;
use Validator;

class QuestionarioController extends Controller
{
    public function index(){
        $data['questionarios'] = Questionario::get();

        return view('app.trilhas.quiz', $data);
    }



    public function novoquestionario (Request $request) {

        $validator = Validator::make($request->all(), [
            'questionario_titulo' => 'required',
            'questionario_descricao' => 'required',
            'questionario_perguntas_descricao' => 'required'
        ], [
            'required' => 'O campo :attribute não foi preenchido',
        ]);


        if ($validator->fails()) {
            var_dump('erro de validação dos dados');
            exit;
        }

        $perguntas = [];
        foreach ($request->questionario_perguntas_descricao as $item) {
            array_push($perguntas, ['descricao' => $item]);
        }

        $questionario = new Questionario();
        $questionario->usuarioAdmId = Auth::user()->id;
        $questionario->status = Questionario::STATUS_ACTIVE;
        $questionario->titulo = $request->questionario_titulo;
        $questionario->descricao = $request->questionario_descricao;

        $questionario->save();
        $questionario->perguntas()->createMany($perguntas);


        return redirect(route('app::trilha::questionarios'));

    }

    public function viewListarQuestionarios () {
        return view('app.trilhas.listquiz');
    }

    public function respostas($id){
        $questionario = Questionario::find($id)->with('perguntas')->get();

        echo "<pre>";
        print_r($questionario);
        echo "</pre>";
    }
}
