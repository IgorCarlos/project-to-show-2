<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Usuario;
use App\Model\UsuarioAdm;
use Illuminate\Support\Facades\Hash;

class AutenticacaoAdminController extends Controller
{

    public function viewEntrar (Request $request) {

        return view('app.login.index');

    }

    public function entrar (Request $request) {

        $usuario = Usuario::where(['email' => $request->email, 'status' => Usuario::USUARIO_ATIVO])->first();

        // usuario não encontrado
        if (empty($usuario)) return redirect()->route('login')->with('mensagem', 'Credenciais inválidas');

        // valida a senha
        if (!Hash::check($request->senha, $usuario->password)) return redirect()->route('login')->with('mensagem', 'Senha inválida');

        $usuarioAdm = UsuarioAdm::find($usuario->id);

        // valida se o usuário é admin
        if (empty($usuarioAdm)) return redirect()->route('login')->with('mensagem', 'Usuario sem permissão');
        
        // loga
        Auth::login($usuario, false);
        
        // redireciona para a tela inicial
        return redirect('/');
        
    }

    public function sair () {

        Auth::logout();

        return redirect()->route('login');

    }

}
