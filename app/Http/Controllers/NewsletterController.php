<?php

namespace App\Http\Controllers;

use App\Helper\Resposta;
use App\Model\Newsletter;
use Illuminate\Http\Request;
use Validator;

class NewsletterController extends Controller
{

    public function viewLista () {

        return view('app.newsletter.listar', ['newsletters' => Newsletter::get()]);
    }


    // api
    public function add (Request $request) {

        $res = new Resposta();

        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|unique:newsletters',
                'phone' => '',
                'isStudent' => 'boolean',
            ],[
                'required' => 'O campo :attribute não foi preenchido',
            ]);

            // error validate
            if ($validator->fails()) throw new \Exception($validator->errors(), 400);

            // newsletter create
            $body = $request->all();
            $body['status'] = Newsletter::STATUS_ACTIVE;
            $newsletter = Newsletter::create($body);

            $res->setMensagem('Usuário registrado na newsletter com sucesso');
            $res->setDados($newsletter);

            return response($res->getResposta(), 200);


        } catch (\Exception $e) {
            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());
        }

    }


    // ativar
    public function ativar (Request $request) {

        $newsletter = Newsletter::find($request->id);
        $newsletter->status = Newsletter::STATUS_ACTIVE;
        $newsletter->save();

        flash('Newsletter ativada com sucesso')->success();

        return redirect(route('app::newsletter::viewLista'));

    }

    // inativar
    public function inativar (Request $request) {

        $newsletter = Newsletter::find($request->id);
        $newsletter->status = Newsletter::STATUS_INACTIVE;
        $newsletter->save();

        flash('Newsletter inativada com sucesso')->success();

        return redirect(route('app::newsletter::viewLista'));

    }

}
