<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UsuarioComum;
use App\Model\Token;
use App\Helper\Resposta;

class HistoricoController extends Controller
{
    public function watched(Request $req, $tab){

        $res = new Resposta();

        try{
            $hash = $req->header('Authorization');
    
            $usuario = Token::with('usuarioComum')->find($hash);
    
            $usuario->usuarioComum->tabVideo = $tab;
    
            $usuario->usuarioComum->update();
                
            $res->setMensagem("Dados alterados com sucesso");

            return Response($res->getResposta(), 200);

        }catch(\Exception $e){
            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            
            return Response($res->getResposta(), 200);
        }
        
    }
}
