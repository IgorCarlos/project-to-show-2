<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Polo;
use Storage;

class PoloController extends Controller
{
    public function viewNovo(){
        return view('app.polo.novo');
    }
    public function novo(Request $req){

        try{

            $objPolo = new Polo();
            $imagem = $req->file('imagem')->store('public/polos');

            $polo['nomePolo'] = $req->input('titulo');
            $polo['logo'] = $imagem;

            $objPolo->create($polo);

            flash('Polo criado com sucesso')->success();

            return redirect(route('app::polo::viewListar'));
        }catch(\Exception $e){
            echo $e->getMessage();
        }

    }

    public function viewListar(){
        $polos = Polo::paginate(10);

        foreach ($polos as $b){
            $url = Storage::url($b->logo);

            $b->imagem = $url;
        }

        $data['polos'] = $polos;

        return view('app.polo.listar', $data);
    }
}
