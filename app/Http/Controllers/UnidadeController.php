<?php

namespace App\Http\Controllers;

use App\Model\Polo;
use App\Model\Unidade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Model\Evento;
class UnidadeController extends Controller
{
    public function viewNovo(){
        $data['polos'] = Polo::where('status', 1)->get();
        return view('app.unidade.novo', $data);
    }

    public function novo(Request $req){

        $input = $req->input();

        $polo = Polo::find($input['polo']);

        print_r($input);

        $unidade['nome'] = $polo->nomePolo;
        $unidade['endereco'] = $input['rua'];
        $unidade['cidade'] = $input['cidade'];
        $unidade['bairro'] = $input['bairro'];
        $unidade['estado'] = $input['estado'];

        $unidade['senha'] = Hash::make('pauta2327');


        $polo->unidades()->create($unidade);

        flash('Unidade adicionada com sucesso')->success();
        return redirect(route('app::unidade::viewNovo'));

    }

    public function getAPI(){
        $unidades = Evento::find(1)->with('unidades')->first();

        return $unidades;
    }

    public function viewListar(){
        $data['unidades'] = Unidade::paginate(10);

        return view('app.unidade.lista',$data);
    }

    
}
