<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\UsuarioAdmController;
use Validator;
use App\Model\UsuarioAdm;
use App\Model\Usuario;
use Hash;
use App\Model\UsuarioComum;
class UsuarioController extends Controller
{
    public function viewUsuarios(){
        $usuarios = UsuarioAdm::with('usuario')->paginate(10);

        $data['usuarios'] = $usuarios;
        $data['total'] = count($usuarios);

        return view('app.usuarios.listar', $data);
    }

    public function vieEditar(){

        return view('app.usuarios.editar');
    }


    public function viewCadastrar(){
        return view('app.usuarios.novo');
    }

    public function cadastra (Request $request) {


        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
//            'sobrenome' => 'required|string',
            'email' => 'required|email|unique:usuarios',
            'senha' => 'required',
//            'status' => 'numeric'
        ],[
            'required' => 'O campo :attribute não foi preenchido',
            'string' => 'O campo :attribute deve ser do tipo string',
            'email' => 'O campo :attribute deve conter um formato de email',
            'numeric' => 'O campo :attribute deve ser do tipo numérico',
            'unique' => 'Esté usuário ja foi cadastrado'
        ]);

        if ($validator->fails()) {
            return redirect(route('app::usuario::viewCadastra'))->withErrors($validator)->withInput();

        }

        $body = $request->all();
        $body['status'] = Usuario::USUARIO_ATIVO;
        $body['senha'] = Hash::make($request->senha);

        try {

            $usuario = Usuario::create($body);
            $usuario->usuarioAdm()->create($body);

            return redirect(route('app::usuario::viewLista'));
        } catch(\Exception $e) {

            print_r($e->getMessage());

        }
    }

    public function inativar($id){

        try{
            $usuarioAdm = UsuarioAdm::find($id);

            $usuarioAdm->usuario->inativa();

            return redirect(route('app::usuario::viewLista'));
        }catch(\Exception $e){

        }

    }


    public function ativar($id){

        try{
            $usuarioAdm = UsuarioAdm::find($id);

            $usuarioAdm->usuario->ativa();

            return redirect(route('app::usuario::viewLista'));
        }catch(\Exception $e){

        }
    }

    public function listaComum(){
        $dados['usuarios'] = UsuarioComum::with('usuario')->paginate(10);

        return view('app.users.lista', $dados);
    }

    public function viewPerfilUser(){
        return view('app.users.perfil');
    }
}
