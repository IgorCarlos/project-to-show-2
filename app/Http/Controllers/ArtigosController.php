<?php

namespace App\Http\Controllers;

use App\Model\ArtigoTag;
use App\Model\Tag;
use Illuminate\Http\Request;
use App\Helper\Resposta;
use App\Http\Requests\CadastraArtigo;
use App\Http\Requests\AtualizaArtigo;
use App\Model\Artigo;
use App\Model\ArtigoConteudo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class ArtigosController extends Controller
{

    // 
    public function viewCadastra (Request $request) {

        return view('app.artigo.cadastra', [
            'tags' => Tag::get(),
            'artigo' => !empty($request->id) ? Artigo::find($request->id) : ""
        ]);
    }

    public function viewListar () {

        return view('app.artigo.listar', ['artigos' => Artigo::paginate(5)]);
    }


    // cadastra artigo
    public function cadastra (Request $request) {

        $validator = Validator::make($request->all(), [
            'titulo' => 'required',
            'conteudo_curto' => 'required',
            'conteudo' => 'required',
            'tags' => 'required'
        ],[
            'required' => 'O campo :attribute não foi preenchido',
            'image' => 'O arquivo não é imagem',
        ]);


        if($validator->fails()){
            return redirect(route('app::artigo::viewCadastrar'))->withErrors($validator)->withInput();
        }

        $body = $request->all();

        // resolvendo o banner
        if (!empty($request->banner)) {
            $imagem = $request->file('banner')->store('public/artigos');
            $body['banner'] = $imagem;
        } else {
            unset($body['banner']);
        }

        $usuario = Auth::user();

        // resolvendo os dados comuns
        $body['usuarioAdmId'] = $usuario->id;
        $body['status'] = Artigo::ARTIGO_ACTIVE;
        $body['destaque'] = empty($body['destaque']) ? 0 : 1;

        // resolvendo as tags
        $arrayTag = [];
        foreach ($body['tags'] as $tag) {
             array_push($arrayTag, ['tagId' => $tag]);
        }

        // resolvendo a url do banner
        $body['link'] = implode('-', explode(' ', strtolower($request->titulo)));

        // resolvendo se o usuário está logado ou não para atualizar ou criar o banner
        if (!isset($request->id) || empty($request->id)) {
            $artigo = Artigo::create($body);
        } else {
            $artigo = Artigo::find($request->id);
            $artigo->update($body);
            $artigo->artigoTag()->delete();
        }

        $artigo->artigoTag()->createMany($arrayTag);

        flash("Artigo cadastrado com sucesso")->success();

        return redirect(route('app::artigo::viewCadastra'));

    }


    // "metodos de repositório"

    public function pegaUm (Request $request) {

        $res = new Resposta();

        try {

            $artigo = Artigo::where([
                'id' => $request->id,
                'status' => Artigo::ARTIGO_ACTIVE
            ])
                ->with(['tags', 'comentarios', 'usuarioAdm'])
                ->first();

            if (empty($artigo)) {
                return response(null, 204);
            }

            if ($artigo->status == Artigo::ARTIGO_INACTIVE) {
                $res->setMensagem('Artigo inativo');
                return response($res->getResposta, 204);
            }

            $artigo['capa'] = url('/').Storage::url($artigo['banner']);
            
            $res->setDados($artigo);
            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());
            return response($res->getResposta(), $e->getCode());

        }

    }

    public function pegaTodos (Request $request) {

        $res = new Resposta();

        $offset = 0;
        $limit  = 20;

        try {

            if (isset($request->offset) && !empty($request->offset)) {
                $offset = $request->offset;
            }

            if (isset($request->limit) && !empty($request->limit)) {
                $limit = $request->limit;
            }


            $artigos = Artigo::where('status', Artigo::ARTIGO_ACTIVE)
                ->with(['tags', 'comentarios', 'usuarioAdm'])
                ->offset($offset)
                ->limit($limit)
                ->get();


            foreach ($artigos as $artigo) {
                $artigo->banner =  url('/').Storage::url($artigo->banner);
            }


            $res->setDados($artigos);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            return response($res->getResposta(), $e->getCode());

        }

    }

    public function pegaTodosDestaque (Request $request) {

        $res = new Resposta();
        $offset = 0;
        $limit  = 20;


        try {

            if (isset($request->offset) && !empty($request->offset)) {
                $offset = $request->offset;
            }

            if (isset($request->limit) && !empty($request->limit)) {
                $limit = $request->limit;
            }

//            $artigos = Artigo::where(['status' => Artigo::ARTIGO_ACTIVE, 'destaque' => Artigo::DESTAQUE_TRUE])->get();

            $artigos = Artigo::where(['status' => Artigo::ARTIGO_ACTIVE, 'destaque' => Artigo::DESTAQUE_TRUE])
                ->with(['tags', 'comentarios', 'usuarioAdm'])
                ->offset($offset)
                ->limit($limit)
                ->get();

            foreach ($artigos as $artigo) {
                $artigo->banner =  url('/').Storage::url($artigo->banner);
            }

            $res->setDados($artigos);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            return response($res->getResposta(), $e->getCode());

        }
    }

    public function pegaTodosSemDestaque (Request $request) {

        $res = new Resposta();
        $offset = 0;
        $limit  = 20;


        try {

//            $artigos = Artigo::where(['status' => Artigo::ARTIGO_ACTIVE, 'destaque' => Artigo::DESTAQUE_FALSE])->get();


            if (isset($request->offset) && !empty($request->offset)) {
                $offset = $request->offset;
            }

            if (isset($request->limit) && !empty($request->limit)) {
                $limit = $request->limit;
            }


            $artigos = Artigo::where(['status' => Artigo::ARTIGO_ACTIVE, 'destaque' => Artigo::DESTAQUE_FALSE])
                ->with(['tags', 'comentarios', 'usuarioAdm'])
                ->offset($offset)
                ->limit($limit)
                ->get();


            foreach ($artigos as $artigo) {
                $artigo->banner =  url('/').Storage::url($artigo->banner);
            }

            $res->setDados($artigos);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setMensagem($e->getMessage());
            $res->setErro(true);

            return response($res->getResposta(), $e->getCode());

        }
    }


    public function ativa (Request $request) {
        
        $res = new Resposta();

        try {

            if (empty($request->id)) throw new \Exception('Necessário parâmetro ID', 400);

            $artigo = Artigo::find($request->id);

            if (empty($artigo)) throw new \Exception('Nenhum artigo encontrado', 400);

            $artigo->status = Artigo::ARTIGO_ACTIVE;

            $res->setMensagem("Artigo {$artigo->titulo} ativado com sucesso");
            $res->setDados($artigo);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());

            return response($res->getResposta(), $e->getCode());

        }

    }

    public function inativa (Request $request) {
        
        $res = new Resposta();

        try {

            if (empty($request->id)) throw new \Exception('Necessário parâmetro ID', 400);

            $artigo = Artigo::find($request->id);

            if (empty($artigo)) throw new \Exception('Nenhum artigo encontrado', 400);

            $artigo->status = Artigo::ARTIGO_INACTIVE;

            $res->setMensagem("Artigo {$artigo->titulo} inativado com sucesso");
            $res->setDados($artigo);

            return response($res->getResposta(), 200);

        } catch (\Exception $e) {

            $res->setErro(true);
            $res->setMensagem($e->getMessage());

            return response($res->getResposta(), $e->getCode());

        }

    }

}
