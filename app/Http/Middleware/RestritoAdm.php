<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Model\UsuarioAdm;

class RestritoAdm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        // caso o usuário não estiver autenticado
        if (Auth::guest()) return redirect()->route('login');

        // pega o usuario autenticado e verifica se é adm
        $usuarioAuth = Auth::user();

        // busca usuario logado em administrador
        $usuarioAdm = UsuarioAdm::find($usuarioAuth->id);

        // valida se o usuário é administrador
        if (empty($usuarioAdm)) {
            Auth::logout();
            return redirect()->route('login')->with('mensagem', 'Usuario sem permissão');
        }

        return $next($request);
    }
}
