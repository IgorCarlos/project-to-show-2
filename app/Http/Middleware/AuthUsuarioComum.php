<?php

namespace App\Http\Middleware;

use App\Model\Usuario;
use Closure;
use App\Helper\Resposta;
use App\Model\UsuarioComum;
use App\Model\Token;

class AuthUsuarioComum
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $res = new Resposta();

        try {

            $token = $request->header('Authorization');
            //$cpf = $request->header('token');

            // valida se foi passado o token no header
            if (empty($token)) throw new \Exception('Verifique as credenciais informadas', 400);

            // busca o usuário pelo token
            $usuarioComum = UsuarioComum::with('token')->whereHas('token', function ($query) use ($token) {
                $query->where('hash', $token);
            })->first();

            // valida se o usuário existe
            if (empty($usuarioComum)) throw new \Exception('Nenhum usuário encontrado', 200);

            // valida se o token esta expirado
            Token::isValid($usuarioComum->token);

            return $next($request)->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                ->header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

        } catch (\Exception $e) {
            $res->setErro(true);
            $res->setMensagem('Você não está logado');
            $res->setDados(['logged' => false]);
            return response($res->getResposta(), 200);
        }

    }
}
