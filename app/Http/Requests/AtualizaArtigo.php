<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MensagensPadrao;

class AtualizaArtigo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuarioAdmId' => 'numeric|exists:usuarios_adm,usuarioId',
            'imagem' => 'string',
            'banner' => 'string',
            'titulo' => 'string|max:150',
            'status' => 'numeric',
            'conteudo' => 'string',
            'multimidia' => 'string',
        ];
    }

    public function messages () {
        return MensagensPadrao::MENSAGENS;
    }
}
