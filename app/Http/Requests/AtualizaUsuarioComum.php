<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MensagensPadrao;


class AtualizaUsuarioComum extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'desafioAtual' => 'numeric',
            'cpf' => [
                'regex:/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/'
            ],
            'idade' => 'numeric|max:99',
            'matriculado' => 'numeric',
            'nome' => 'string',
            'sobrenome' => 'string',
            'email' => 'email',
            'senha' => '',
            'status' => 'numeric',
            'created_at' => 'date'
        ];

    }

    public function messages () {
        return MensagensPadrao::MENSAGENS;
    }
    
}
