<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MensagensPadrao;

class CadastraArtigo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuarioAdmId' => 'required|numeric|exists:usuarios_adm,usuarioId',
            'imagem' => 'string|required|max:255',
            // 'banner' => '',
            'titulo' => 'required|string|max:150',
            'status' => 'required|numeric',
            // 'created_at' => 'required|date',
            // 'updated_at' => '',
            'conteudo' => 'required|string',
            // 'multimidia' => '',
        ];
    }

    public function messages () {
        return MensagensPadrao::MENSAGENS;
    }
}
