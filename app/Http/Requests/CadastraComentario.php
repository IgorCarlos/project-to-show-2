<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MensagensPadrao;

class CadastraComentario extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'artigoId' => 'required|numeric|exists:artigos,id',
            'usuarioComumId' => 'required|numeric|exists:usuarios_comum,usuarioId',
            'comentario' => 'required'
        ];
    }

    public function messages () {
        return MensagensPadrao::MENSAGENS;
    }
}
