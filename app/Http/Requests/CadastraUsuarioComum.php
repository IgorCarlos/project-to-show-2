<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MensagensPadrao;


class CadastraUsuarioComum extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'desafioAtual' => 'required',
            'cpf' => [
                'required', 'unique:usuarios_comum', 'numeric',
                'regex:/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/'
            ],
            'idade' => 'required|numeric|max:99',
            'matriculado' => 'required|boolean',
            'nome' => 'required|string',
            'sobrenome' => 'required|string',
            'email' => 'required|email',
            'senha' => 'required',
            'status' => 'numeric',
            // 'created_at' => 'required|date'
        ];
    }

    public function messages () {
        return MensagensPadrao::MENSAGENS;
    }

}
