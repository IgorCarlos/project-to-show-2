<?php 

namespace App\Http\Requests;

class MensagensPadrao {

    const MENSAGENS = [
        'required' =>  ':attribute é um campo obrigatório.',
        'unique' => ':attribute já existe',
        'numeric' => ':attribute deve ser um valor numérico',
        'max' => ':attribute excedeu a quantidade de caracter permitida',
        'boolean' => ':attribute aceita apenas valor como true ou false.',
        'string' => ':attribute deve conter apenas letras',
        'email' => ':attribute não esta em um formato correto',
        'date' => ':attribute não está no formato correto',
        'regex' => ':attribute não está no formato correto',
        'exists' => ':attribute id não é valido'
    ];

}