<?php 

namespace App\Http\Requests;

class CadastraUsuarioRegras {

    const regras = [
        'desafioAtual' => 'required',
        'cpf' => [
            'required', 'unique:usuarios_comum', 'numeric',
            'regex:/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/'
        ],
        'idade' => 'required|numeric|max:99',
        'matriculado' => 'required|boolean',
        'nome' => 'required|string',
        'sobrenome' => 'required|string',
        'email' => 'required|email',
        'senha' => 'required|numeric',
        'status' => 'numeric',
        'created_at' => 'required|date'
    ];

}