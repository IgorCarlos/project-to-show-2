<?php

namespace App\Mail;

use App\Model\Token;
use App\Model\UsuarioComum;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestRecoveryPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UsuarioComum $usuarioComum)
    {
        $this->usuarioComum = $usuarioComum;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.recoveryPassword')
            ->subject('Recuperar senha | Carreira em pauta')
            ->from('carreiraempauta@suporte.com')
            ->with(['usuarioComum' => $this->usuarioComum, 'host' => 'http://carreiraempauta.corebiz.com.br']);
    }
}
