<?php 

namespace App\Helper;

class Resposta
{
    private $erro;
    private $mensagem;
    private $dados;

    public function __construct ($erro = false, $mensagem = '', $dados = []) {

        $this->erro = $erro;
        $this->mensagem = $mensagem;
        $this->dados = $dados;

    }

    public function setErro ($erro) {
        $this->erro = $erro;
    }

    public function setMensagem ($mensagem) {
        $this->mensagem = $mensagem;
    }

    public function setDados ($dados) {
        $this->dados = $dados;
    }

    public function setResposta ($erro, $mensagem, $dados) {

        $this->erro = $this->setErro($erro);
        $this->mensagem = $this->setMensagem($mensagem);
        $this->dados = $this->setDados($dados);
        
    }

    public function getResposta () {

        return [
            'error' => $this->erro,
            'message' => $this->mensagem,
            'data' => $this->dados
        ];

    }    

}
