<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// usuario -- comum
Route::post('/usuario', ['uses' => 'UsuarioComumController@cadastra'])->middleware('cors');
Route::get('/usuario/{id}', ['uses' => 'UsuarioComumController@pegaUm'])->middleware('cors');
Route::put('/usuario/{id}', ['middleware' => 'authUsuarioComum','uses' => 'UsuarioComumController@atualiza'])->middleware('cors');
Route::get('/usuario', ['uses' => 'UsuarioComumController@pegaTodos'])->middleware('cors');
Route::put('/usuario/{id}/ativar', ['uses' => 'UsuarioComumController@ativa'])->middleware('cors');
Route::put('/usuario/{id}/inativar', ['uses' => 'UsuarioComumController@inativa'])->middleware('cors');

// recuperar senha
Route::post('/usuario/recuperar', ['uses' => 'UsuarioComumController@recoveryPassword'])->middleware('cors');
Route::post('/usuario/novasenha', ['uses' => 'UsuarioComumController@newPassword'])->middleware('cors');
Route::get('/mailable', function () {

    $usuarioComum = \App\Model\UsuarioComum::with('usuario', 'token')
        ->where('usuarioId', 21)
        ->first();

    return new \App\Mail\RequestRecoveryPassword($usuarioComum);

});

// usuario -- adm
Route::post('/administrador', ['uses' => 'UsuarioAdmController@cadastra'])->middleware('cors');
Route::get('/administrador/{id}', ['uses' => 'UsuarioAdmController@pegaUm'])->middleware('cors');
Route::put('/administrador/{id}', ['uses' => 'UsuarioAdmController@atualiza'])->middleware('cors');
Route::get('/administrador', ['uses' => 'UsuarioAdmController@pegaTodos'])->middleware('cors');
Route::put('/administrador/{id}/ativar', ['uses' => 'UsuarioAdmController@ativa'])->middleware('cors');
Route::put('/administrador/{id}/inativar', ['uses' => 'UsuarioAdmController@inativa'])->middleware('cors');

// banners
Route::get('/banner', ['uses' => 'bannerController@getBanners'])->middleware('cors');

// artigo 
//Route::post('/artigo', ['uses' => 'ArtigosController@cadastra'])->middleware('cors');
Route::get('/artigo', ['uses' => 'ArtigosController@pegaTodos'])->middleware('cors');
Route::get('/artigo/destaque', ['uses' => 'ArtigosController@pegaTodosDestaque'])->middleware('cors');
Route::get('/artigo/semdestaque', ['uses' => 'ArtigosController@pegaTodosSemDestaque'])->middleware('cors');
Route::get('/artigo/{id}', ['uses' => 'ArtigosController@pegaUm'])->middleware('cors');
Route::put('/artigo/{id}/ativar', ['uses' => 'ArtigosController@ativa'])->middleware('cors');
Route::put('/artigo/{id}/inativar', ['uses' => 'ArtigosController@inativa'])->middleware('cors');

// comentario
Route::post('/comentario', ['uses' => 'ComentarioController@cadastra'])->middleware('cors');

// login
Route::post('/auth', ['uses' => 'AutenticacaoComumController@autentica'])->middleware('cors');
Route::post('/authFB', ['uses' => 'AutenticacaoComumController@autenticaWithFB'])->middleware('cors');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/getUserLoggedData/', ['uses' => 'AutenticacaoComumController@getData'])->middleware('authUsuarioComum');


//questionario
Route::prefix('trilha')->group(function(){
    Route::post('/', ['uses' => 'TrilhaController@postTrilha'])->middleware('cors');
//    Route::post('/conteudo', ['uses' => 'TrilhaController@adicionaConteudo'])->middleware('cors');
    Route::get('/{id}', ['uses' => 'TrilhaController@apiListar'])->middleware('cors');
});


//banners
Route::get('/banners', ['uses' => 'bannerController@todos'])->middleware('cors');

Route::post('/watched/{tab}', ['uses' => 'AutenticacaoComumController@watched'])->middleware('cors');

Route::post('/trilhapremium/etapa/{etapa}', ['uses' => 'TrilhaPremiumController@addEtapa'])->middleware('cors');

// newsletter
Route::post('/newsletter', ['uses' => 'NewsletterController@add'])->middleware('cors');


// testuser
Route::post('/testeuser', ['uses' => 'AutenticacaoComumController@teste'])->middleware('cors');


// evento
Route::get('/evento', ['uses' => 'EventoController@getAPI'])->middleware('cors') ;


// listen user
Route::get('/autocomplete/curso/{reference}', ['uses' => 'CursoController@listenUser'])->middleware('cors');
Route::get('/cursos/{curso}', ['uses' => 'CursoController@getByCourse'])->middleware('cors');

Route::get('/unidades',['uses' => 'UnidadeController@getAPI'])->middleware('cors');

// trilha resultado
Route::post('/trilha/resultado', ['uses' => 'TrilhaController@saveResult']);

// trilha apagar
Route::post('/trilha/apagar', ['uses' => 'TrilhaController@cleanResult']);


// trilhas for perfil
Route::get('/perfil/testes', ['middleware' => 'cors', 'uses' => 'UsuarioComumController@getAnsweredFromTrail'])->middleware('authUsuarioComum');


// busca
Route::get('/busca/artigos/{reference}', ['middleware' => 'cors', 'uses' => 'BuscaController@busca']);

Route::post('/tp/addEtapa', ['uses' => 'TrilhaPremiumController@addEtapaPremium'])->middleware('cors');


Route::post('/tp/desafioUp', ['uses' => 'TrilhaPremiumController@addRespDesafioUp'])->middleware('cors');

Route::post('/tp/artigos', ['uses' => 'TrilhaPremiumController@artigos'])->middleware('cors');


Route::post('/tp/pdi', ['uses' => 'TrilhaPremiumController@pdi'])->middleware('cors');

