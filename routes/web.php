<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// view
Route::get('/entrar', ['uses' => 'AutenticacaoAdminController@viewEntrar'])->name('login');
Route::post('/auth/administrador', ['uses' => 'AutenticacaoAdminController@entrar'])->name('entrarAdm');
Route::get('/testeintegracao', ['uses' => 'CursoController@listCursos']);

Route::name('app::')->middleware('restritoAdm')->group(function(){

    // home
    Route::get('/', [
        'uses' => 'HomeController@inicio', 'as' => 'inicio'
    ]);

    // banners
    Route::prefix('banners')->name('banner::')->group(function(){

        Route::match(['post','get'],'/novo', ['uses' => 'bannerController@novo'])->name('novo');

        Route::get('/alterarStatus/{id}/{status}', ['uses' => 'bannerController@changeStatus'])->name('status');

        Route::get('/deletar/{id}', ['uses' => 'bannerController@deletar'])->name('deletar');

        Route::get('/', ['uses' => 'bannerController@getBanners'])->name('getBanners');
    });


    // trilhas
    Route::prefix('trilhas')->name('trilha::')->group(function(){
        Route::get('/', ['uses' => 'TrilhaController@listar'])->name('listar');
        Route::get('/editar/{id}', ['uses' => 'TrilhaController@editar'])->name('editar');
        Route::post('/editar/{id}', ['uses' => 'TrilhaController@edit'])->name('editar');
        Route::get('/questionarios/criar', ['uses' => 'QuestionarioController@index'])->name('questionarios');
        Route::post('/questionarios/criar', ['uses' => 'QuestionarioController@novoquestionario'])->name('novoquestionario');
        Route::get('/questionarios/lista', ['uses' => 'QuestionarioController@viewListarQuestionarios'])->name('viewListarQuestionarios');
    });


    // administrador
    Route::prefix('perfil')->name('perfil::')->group(function () {

        Route::get('/', ['uses' => 'PerfilAdmController@viewEditar'])->name('viewEditar');
        Route::post('/', ['uses' => 'PerfilAdmController@editar'])->name('editar');
        Route::post('/atualizar-senha', ['uses' => 'PerfilAdmController@atualizarSenha'])->name('atualizarSenha');
        
    });

    //modulo de usuários 
    Route::prefix('administradores')->name('usuario::')->group(function(){

        Route::get('/', ['uses' => 'UsuarioController@viewUsuarios'])->name('viewLista');

        Route::get('/editar/{id}', ['uses' => 'UsuarioController@vieEditar'])->name('viewEditar');

        Route::get('/novo/', ['uses' => 'UsuarioController@viewCadastrar'])->name('viewCadastra');

        Route::post('/cadastra', ['uses' => 'UsuarioController@cadastra'])->name('cadastra');

        Route::get('/inativa/{id}', ['uses' => 'UsuarioController@inativar'])->name('inativar');
        Route::get('/ativa/{id}', ['uses' => 'UsuarioController@ativar'])->name('ativar');

    });


    //usuarios comum na área administrativa
    Route::prefix('usuarios')->name('user::')->group(function(){
        Route::get('/', ['uses' => 'UsuarioController@listaComum'])->name('viewLista');

        Route::get('/perfil/{id}', ['uses' => 'UsuarioController@viewPerfilUser'])->name('viewPerfilUser');

    });


    Route::prefix('cursos')->name('curso::')->group(function(){
        Route::get('/novo', ['uses' => 'CursoController@viewAddCurso'])->name('viewCadastrar');

        Route::get('/', ['uses' => 'CursoController@listar'])->name('viewListar');

        Route::get('/lista-pos', ['uses' => 'CursoController@listarpos'])->name('listarpos');

        Route::post('/novo', ['uses' => 'CursoController@addCurso'])->name('cadastrar');

        Route::get('/novo-pos', ['uses' => 'CursoController@viewCadastrarPos'])->name('viewCadastrarPos');

        Route::post('/novo-pos', ['uses' => 'CursoController@CadastrarPos'])->name('CadastrarPos');

        Route::get('/curso/{id}', ['uses' => 'CursoController@vercurso'])->name('vercurso');

        Route::post('/addpos/{idCurso}', ['uses' => 'CursoController@adicionaPosAoCurso'])->name('adcionaPos');

        Route::get('/detachPos/{idCurso}/{idPos}', ['uses' => 'CursoController@detachPos'])->name('detachPos');

        Route::get('/softDelete/{id}', ['uses' => 'CursoController@softDelete'])->name('softDelete');

    });


    // newsletter
    Route::prefix('newsletter')->name('newsletter::')->group(function(){

        Route::get('/', ['uses' => 'NewsletterController@viewLista'])->name('viewLista');
        Route::post('/ativar', ['uses' => 'NewsletterController@ativar'])->name('ativar');
        Route::post('/inativar', ['uses' => 'NewsletterController@inativar'])->name('inativar');

    });




    // artigo
    Route::prefix('artigo')->name('artigo::')->group(function () {

        Route::get('/cadastrar', ['uses' => 'ArtigosController@viewCadastra'])->name('viewCadastra');
        Route::post('cadastrar', ['uses' => 'ArtigosController@cadastra'])->name('cadastra');
        Route::post('ativar', ['uses' => 'ArtigosController@ativar'])->name('ativar');
        Route::post('inativar', ['uses' => 'ArtigosController@inativar'])->name('inativar');
        Route::get('/lista', ['uses' => 'ArtigosController@viewListar'])->name('viewLista');

    });

    // tags
    Route::prefix('tags')->name('tags::')->group(function () {

        Route::get('cadastrar', ['uses' => 'TagsController@viewCadastra'])->name('viewCadastra');
        Route::get('lista', ['uses' => 'TagsController@viewListar'])->name('viewLista');
        Route::post('cadastrar', ['uses' => 'TagsController@cadastra'])->name('cadastra');
        Route::post('inativar', ['uses' => 'TagsController@inativar'])->name('inativar');
        Route::post('ativar', ['uses' => 'TagsController@ativar'])->name('ativar');

    });

    Route::prefix('evento')->name('evento::')->group(function(){

        Route::get('/', ['uses' => 'EventoController@index'])->name('inicio');

        Route::get('/novo', ['uses' => 'EventoController@viewNovo'])->name('viewNovo');
        Route::post('/novo', ['uses' => 'EventoController@novo'])->name('novo');

        Route::get('/unidades/{id}', ['uses' => 'EventoController@verunidades'])->name('unidades');
        Route::post('/unidades/{id}', ['uses' => 'EventoController@salvarDatas'])->name('salvar');

        Route::get('{id}/inscritos', ['uses' => 'EventoController@verInscritos'])->name('inscritos');
        Route::get('/inscritos/{userId}', ['uses' => 'EventoController@confirmarInscrito'])->name('inscrito::confirmar');
        Route::get('{id}/inscritos/xls', ['uses' => 'EventoController@gerarXls'])->name('exportar');


        Route::get('/midias', ['uses' => 'EventoController@verMidias'])->name('midias');
        Route::get('/midias/nova', ['uses' => 'EventoController@novaMidia'])->name('novaMidia');
        Route::post('/midias/nova', ['uses' => 'EventoController@addMidia'])->name('addMidia');
        Route::get('/midias/excluir/{id}', ['uses' => 'EventoController@excluirMidia'])->name('excluirMidia');

        Route::get('/fotos', ['uses' => 'EventoController@verFotos'])->name('fotos');
        Route::post('/fotos/adicionar', ['uses' => 'EventoController@addFotos'])->name('addfotos');


    });


    Route::prefix('polo')->name('polo::')->group(function(){

        Route::get('/', ['uses' => 'PoloController@viewListar'])->name('viewListar');
        Route::get('/novo', ['uses' => 'PoloController@viewNovo'])->name('viewCadastrar');
        Route::post('/novo', ['uses' => 'PoloController@novo'])->name('novo');
        Route::get('/status/{id}/{status}', ['uses' => 'PoloController@status'])->name('status');

    });


    Route::prefix('unidades')->name('unidade::')->group(function(){

        Route::get('/',['uses' => 'UnidadeController@viewListar'])->name('viewListar');
        Route::get('/novo',['uses' => 'UnidadeController@viewNovo'])->name('viewNovo');
        Route::post('/novo',['uses' => 'UnidadeController@novo'])->name('novo');

    });

    // logout
    Route::get('/administrador/sair', ['uses' => 'AutenticacaoAdminController@sair'])->name('sairAdm');

});

Route::get('/banners/lista', ['uses'=>'bannerController@lista'])->name('lista');

