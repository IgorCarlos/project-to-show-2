
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset(url('/').storage_path(Auth::user()->imagem))}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#">Administrador</a>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU PRINCIPAL</li>
            <!-- <li class="active treeview menu-open"> -->
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-file-image-o"></i> <span>Banners</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::banner::getBanners') }}"><i class="fa fa-list"></i> Todos os banners</a></li>--}}
                    {{--<li><a href="{{ route('app::banner::novo') }}"><i class="fa fa-plus-circle"></i> Novo banner</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-sticky-note-o"></i> <span>Trilhas</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::trilha::listar') }}"><i class="fa fa-list"></i> Listar trilhas </a></li>--}}
                    {{--<li><a href="{{ route('app::trilha::questionarios') }}"><i class="fa fa-plus-circle"></i>Novo Questionário</a></li>--}}
                    {{--<li><a href="{{ route('app::trilha::viewListarQuestionarios') }}"><i class="fa fa-plus-circle"></i>Listar Questionários</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-sticky-note-o"></i> <span>Artigos</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::artigo::viewLista') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                    {{--<li><a href="{{ route('app::artigo::viewCadastra') }}"><i class="fa fa-plus-circle"></i> Adicionar </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-sticky-note-o"></i> <span>Tags</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::tags::viewLista') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                    {{--<li><a href="{{ route('app::tags::viewCadastra') }}"><i class="fa fa-plus-circle"></i> Adicionar </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-user-circle-o"></i> <span>Administradores</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::usuario::viewLista') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                    {{--<li><a href="{{ route('app::usuario::viewCadastra') }}"><i class="fa fa-plus-circle"></i> Adicionar </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-users"></i> <span>Usuários</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::user::viewLista') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                    {{--<li><a href="{{ route('app::usuario::viewCadastra') }}"><i class="fa fa-plus-circle"></i> Adicionar </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-file-text-o"></i> <span>Newsletter</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::newsletter::viewLista') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}


            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-file-text-o"></i> <span>Unidades</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::unidade::viewListar') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                    {{--<li><a href="{{ route('app::unidade::viewNovo') }}"><i class="fa fa-plus-circle"></i> Adicionar </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-file-text-o"></i> <span>Polos</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::polo::viewListar') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                    {{--<li><a href="{{ route('app::polo::viewCadastrar') }}"><i class="fa fa-plus-circle"></i> Adicionar </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="treeview menu">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-file-text-o"></i> <span>Pós Ideal</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('app::curso::viewListar') }}"><i class="fa fa-list"></i> Ver todos </a></li>--}}
                    {{--<li><a href="{{ route('app::curso::viewCadastrar') }}"><i class="fa fa-plus-circle"></i> Adicionar Curso </a></li>--}}
                    {{--<li><a href="{{ route('app::curso::listarpos') }}"><i class="fa fa-list"></i> Listar Pós </a></li>--}}
                    {{--<li><a href="{{ route('app::curso::viewCadastrarPos') }}"><i class="fa fa-plus-circle"></i> Adicionar Pós </a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li class="treeview menu">
                <a href="#">
                    <i class="fa fa-file-text-o"></i> <span>O evento</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('app::evento::inicio')}}"><i class="fa fa-list"></i> Ver todos </a></li>
                    <li><a href="{{route('app::evento::midias')}}"><i class="fa fa-list"></i> Ver Noticias </a></li>
                    <li><a href="{{ route('app::evento::novaMidia') }}"><i class="fa fa-plus-circle"></i> Adicionar Noticias </a></li>
                    <li><a href="{{route('app::evento::fotos')}}"><i class="fa fa-list"></i> Fotos </a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
