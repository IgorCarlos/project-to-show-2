@if(count( $errors ) > 0 )
    @foreach ($errors->all() as $error)
        <div class="callout callout-danger cleanMargin">
            <p>{{$error}}</p>
        </div>
    @endforeach
@endif
