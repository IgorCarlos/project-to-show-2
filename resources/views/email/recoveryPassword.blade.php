<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
{{--    <link rel="stylesheet" href="{{ asset('css/emailRecoveryPassword.css') }}">--}}
    <title>Recupere sua senha</title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto');

        * {
            font-family:'Roboto', Sans-Serif;
            font-weight:normal;
        }

        body {
            position: relative;
            background-color: #f5f5f5;
            font-size:12pt;
        }

        #area-recovery-password {
            position: relative;
            display: block;
            max-width:700px;
            background-color: #fff;
            margin:0 auto;
            box-shadow: 0 2px 2px #e1e1e1;
        }


        .recovery-password-header {
            position: relative;
            display: block;
            width:100%;
            padding:30px 10px;
            margin:0 auto;
            border-bottom:1px solid #f5f5f5;
        }

        .recovery-password-header-logo {
            display: block;
            width:auto;
            margin:0 auto;
        }

        .recovery-password-content {
            display: block;
            width:100%;
        }

        .recovery-password-title {
            text-align: center;
            padding-bottom:10px;
        }

        .recovery-password-body {
            padding:5px 20px;
        }

        .recovery-password-text-light {
            color: #666;
        }

        .recovery-password-action-button {
            display: block;
            padding:13px 30px;
            background-color: #67A1EE;
            color: #fff0ff;
            border: 1px solid #4e83ee;
            cursor: pointer;
            margin:40px auto;
            text-transform: uppercase;
            text-align: center;
            width:150px;
            text-decoration: none;
        }

        .recovery-password-footer {
            display: block;
            width:100%;
            height:10px;
            background-color: #f2f2f2;
        }
    </style>
</head>

<body>

    {{--area--}}
    <section id="area-recovery-password">

        {{--header--}}
        <header class="recovery-password-header">
            <img class="recovery-password-header-logo" src="{{ asset('images/logo-carreira.png') }}" alt="logo kroton">
        </header>

        {{--conteudo--}}
        <div class="recovery-password-content">

            <header>
                <h1 class="recovery-password-title">Recuperar senha</h1>
            </header>

            <div class="recovery-password-body">
                <p>Olá {{ explode(' ', $usuarioComum->usuario->name)[0] }},</p>
                <p>Você esqueceu sua senha e está recebendo este e-mail para poder gerar outra. <span class="recovery-password-text-light">Caso você não tenha solicitado a troca de sua senha, ignore este e-mail.</span></p>

                <a class="recovery-password-action-button" href="{{ $host }}/usuario/recuperar-senha/{{ $usuarioComum->token->hash }}">Recuperar senha</a>

            </div>

        </div>

        {{--footer--}}
        <footer class="recovery-password-footer"></footer>

    </section>
</body>

</html>