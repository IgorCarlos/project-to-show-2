@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Listar
            <small>Tags</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tags</a></li>
            <li class="active">Listar</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            <section class="col-md-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        @include('flash::message')
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                            <th>Nome</th>
                            <th>Status</th>
                            <th>Ações</th>
                            </thead>
                            <tbody>
                            @foreach($tags as $tag)
                                <tr>
                                    <td>{{ $tag->nome }}</td>
                                    <td>{{ $tag->writeStatus($tag->status) }}</td>
                                    <td>
                                        @if ($tag->status == 1)
                                            <form action="{{ route('app::tags::inativar') }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="id" value="{{ $tag->id }}">
                                                <input type="submit" value="Inativar" class="btn btn-warning">
                                            </form>
                                        @else
                                            <form action="{{ route('app::tags::ativar') }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="id" value="{{ $tag->id }}">
                                                <input type="submit" value="Ativar" class="btn btn-primary">
                                            </form>
                                        @endif

                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Ao inativar a categoria ela não será exibida no cadastro de categorias</li>
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>
        </section>
    </section>
@endsection