@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Cadastrar
            <small>Tags</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tags</a></li>
            <li class="active">Cadastrar</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            <section class="col-md-8">
                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                    </div>

                    @include('flash::message')

                    <!-- /.box-header -->
                    <form action="{{ route('app::tags::cadastra') }}" method="post">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="box-body">
                            <div class="form-group">
                                <label for="nome">Tag</label>
                                <input class="form-control" type="text" name="nome" id="nome" placeholder="Digite o nome da tag" required>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Cadastrar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <!-- <li>Caso queira excluir um banner, basta clicar no icone: <i class="fa fa-trash-o fa-3" aria-hidden="true"></i></li> -->
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection