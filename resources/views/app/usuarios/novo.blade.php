@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Novo Usuário
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Banners</a></li>
            <li class="active">Novo</li>
        </ol>
    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastro</h3>
                    </div>
                    @include('flash::message')
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('app::usuario::cadastra')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="titulo">Nome</label>
                                <input type="text" class="form-control" id="titulo" name="name" value="{{ old('name') }}" />
                            </div>

                            <div class="form-group">
                                <label for="titulo">E-mail</label>
                                <input type="text" class="form-control" id="titulo" name="email" value="{{ old('email') }}" />
                            </div>

                            <div class="form-group">
                                <label for="titulo">Senha</label>
                                <input type="password" class="form-control" id="senha" name="senha" value="{{ old('senha') }}" />
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>O campo titulo é obrigatório</li>
                            <li>A imagem é obrigatória e deve ter dimensão de: 1920x637 px</li>
                            <li>A imagem deve estar nas seguintes extensões: jpeg, png, bmp, gif, ou svg</li>
                        </ul>
                    </div>
                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection