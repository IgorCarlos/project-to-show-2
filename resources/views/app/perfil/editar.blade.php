@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Perfil
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-profile"></i> Home</a></li>
            <li class="active">Perfil</li>
        </ol>
    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Editar dados básicos</h3>
                    </div>
                    @include('flash::message')
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('app::perfil::editar')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="box-body">

                            <div class="form-group">

                                {{--<img src="{{ asset($administrador->imagem) }}" alt="{{ $administrador->name }}">--}}

                                <label for="titulo">Nome</label>
                                <input type="text" class="form-control" id="nome" name="name" value="{{ $administrador->name }}" required>

                                <label for="imagem">Imagem</label>
                                <input type="file" name="imagem" id="imagem">

                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" name="email" id="email" value="{{ $administrador->email }}" required>

                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Atualizar dados</button>
                            </div>

                        </div>
                    </form>

                    <form action="{{ route('app::perfil::atualizarSenha') }}" method="post">
                        {{ csrf_field() }}
                        <div class="box-header with-border">
                            <h3 class="box-title">Atualizar senha</h3>
                        </div>

                        <div class="form-group box-body">
                            <label for="senhaAtual">Senha atual</label>
                            <input class="form-control" type="password" name="senhaAtual" id="senhaAtual" required>

                            <label for="senhaNova">Nova senha</label>
                            <input class="form-control" type="password" name="senhaNova" id="senhaNova" required>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-warning">Atualizar senha</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Campos obrigatórios: <strong>nome, sobrenome, e-mail</strong></li>
                            <li>Caso optar por atualizar a senha os <strong>dois campos são obrigatórios</strong></li>
                        </ul>
                    </div>
                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection