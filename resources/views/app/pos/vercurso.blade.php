@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Simple Tables
            <small>preview of simple tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            <section class="col-md-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$curso->nome}}</h3>
                    </div>
                    
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($curso->pos as $cPos)
                                    <tr>
                                        <td>{{$cPos->nome}}</td>
                                        <td><a href="{{route('app::curso::detachPos', ['idCurso' => $curso->id, 'idPos' => $cPos->id])}}">Desvincular</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    <div class="box-footer clearfix">

                    </div>
                </div>
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Adicionar Pós relacionada</h3>
                    </div>
                    <div class="box-body">

                        <form action="{{ route('app::curso::adcionaPos', ['idCurso' => $curso->id]) }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group col-md-8">
                                {{--seleção de tags--}}
                                    <label for="pos">Tags</label><br />
                                    <select name="pos[]" id="pos" class="selectpicker" data-live-search="true" multiple title="Selecione as tags deste artigo" required>
                                        @foreach ($pos as $cPos)
                                                @if(!in_array($cPos->id, $ArrPos))
                                                    <option value="{{ $cPos->id }}">
                                                        {{ $cPos->nome }}
                                                    </option>
                                                @endif

                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">

                                    <button type="submit" class="btn btn-primary">Adicionar</button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="box-footer clearfix">

                    </div>
                </div>
            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Caso queira excluir um banner, basta clicar no icone: <i class="fa fa-trash-o fa-3" aria-hidden="true"></i></li>
                            <li>Caso queira editar um banner, basta clicar no icone: <i class="fa fa-pencil" aria-hidden="true"></i></li>
                            <li>Caso queira desativar um banner clique em: <span class="label label-success">Ativo</span></li>
                            <li>Você pode também usar o campo de busca para encontrar um banner pelo titulo.</li>
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>

        </section>
    </section>
@endsection