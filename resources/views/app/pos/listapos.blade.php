@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Simple Tables
            <small>preview of simple tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            <section class="col-md-8">
                <div class="box">

                    @include('flash::message')
                    <div class="box-header with-border">
                        <h3 class="box-title">Cursos</h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>Nome</th>
                            </tr>


                            @foreach($cursos as $curso)

                                <tr>
                                    <td>{{$curso->nome}}</td>

                                </tr>

                            @endforeach


                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">


                        {{ $cursos->links() }}
                    </div>
                </div>
            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Caso queira excluir um banner, basta clicar no icone: <i class="fa fa-trash-o fa-3" aria-hidden="true"></i></li>
                            <li>Caso queira editar um banner, basta clicar no icone: <i class="fa fa-pencil" aria-hidden="true"></i></li>
                            <li>Caso queira desativar um banner clique em: <span class="label label-success">Ativo</span></li>
                            <li>Você pode também usar o campo de busca para encontrar um banner pelo titulo.</li>
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>

        </section>
    </section>
@endsection