@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Nova Unidade
        </h1>

    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastro</h3>
                    </div>
                @include('flash::message')
                <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('app::unidade::novo')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-10">
                                    <label for="rua">Rua</label>
                                    <input type="text" name="rua" class="form-control" id="rua" placeholder="Rua">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="numero">Número</label>
                                    <input type="text" name="numero" class="form-control" id="numero" placeholder="Numero">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="cidade">Cidade</label>
                                    <input type="text" name="cidade" class="form-control" id="cidade" placeholder="Titulo">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="bairro">Bairro</label>
                                    <input type="text" name="bairro" class="form-control" id="bairro" placeholder="Titulo">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="titulo" >Estado</label>
                                    <select class="selectpicker" name="estado" id="estado" data-live-search="true" required>
                                        <option value="AC">Acre</option>
                                        <option value="AL">Alagoas</option>
                                        <option value="AP">Amapá</option>
                                        <option value="AM">Amazonas</option>
                                        <option value="BA">Bahia</option>
                                        <option value="CE">Ceará</option>
                                        <option value="DF">Distrito Federal</option>
                                        <option value="ES">Espírito Santo</option>
                                        <option value="GO">Goiás</option>
                                        <option value="MA">Maranhão</option>
                                        <option value="MT">Mato Grosso</option>
                                        <option value="MS">Mato Grosso do Sul</option>
                                        <option value="MG">Minas Gerais</option>
                                        <option value="PA">Pará</option>
                                        <option value="PB">Paraíba</option>
                                        <option value="PR">Paraná</option>
                                        <option value="PE">Pernambuco</option>
                                        <option value="PI">Piauí</option>
                                        <option value="RJ">Rio de Janeiro</option>
                                        <option value="RN">Rio Grande do Norte</option>
                                        <option value="RS">Rio Grande do Sul</option>
                                        <option value="RO">Rondônia</option>
                                        <option value="RR">Rorâima</option>
                                        <option value="SC">Santa Catarina</option>
                                        <option value="SP">São Paulo</option>
                                        <option value="SE">Sergipe</option>
                                        <option value="TO">Tocantins</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="questionario">Polo</label><br>
                                <select name="polo" class="selectpicker">
                                    @foreach($polos as $polo)
                                        <option value="{{$polo->id}}">{{$polo->nomePolo}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>

                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection
