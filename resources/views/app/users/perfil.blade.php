@extends('layouts.app')


@section('content')

    <section class="content">

        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quick Example</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <div class="box-body">

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">

                        </div>
                    </form>
                </div>
                <!-- The time line -->
                <ul class="timeline">
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa bg-blue"></i>

                        <div class="timeline-item">

                            <span class="time"><i class="fa fa-clock-o"></i> 07/10/2017 - 12:05</span>

                            <h3 class="timeline-header"><a href="#">Realizou o cadastro</a></h3>

                        </div>
                    </li>
                    <li>
                        <i class="fa bg-blue"></i>

                        <div class="timeline-item">

                            <span class="time"><i class="fa fa-clock-o"></i> 07/10/2017 - 12:05</span>

                            <h3 class="timeline-header"><a href="#">Finalizou a trilha - Descubra seu sucesso</a></h3>

                        </div>
                    </li>
                    <li>
                        <i class="fa bg-blue"></i>

                        <div class="timeline-item">

                            <span class="time"><i class="fa fa-clock-o"></i> 07/10/2017 - 12:05</span>

                            <h3 class="timeline-header"><a href="#">Finalizou a trilha - Trilhe seu sucesso</a></h3>

                        </div>
                    </li>
                    <li>
                        <i class="fa bg-blue"></i>

                        <div class="timeline-item">

                            <span class="time"><i class="fa fa-clock-o"></i> 07/10/2017 - 12:05</span>

                            <h3 class="timeline-header"><a href="#">Alterou Seus dados</a></h3>

                        </div>
                    </li>
                    <li>
                        <i class="fa bg-blue"></i>

                        <div class="timeline-item">

                            <span class="time"><i class="fa fa-clock-o"></i> 07/10/2017 - 12:05</span>

                            <h3 class="timeline-header"><a href="#">Comprou uma batata</a></h3>

                        </div>
                    </li>
                    <!-- END timeline item -->

                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->



    </section>

    @endsection