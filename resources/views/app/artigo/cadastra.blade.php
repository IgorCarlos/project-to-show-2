@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Cadastrar
            <small>Artigo</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Artigo</a></li>
            <li class="active">Cadastrar</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            <section class="col-md-8">
                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        @include('flash::message')
                    </div>
                    <!-- /.box-header -->
                    <form class="box-body" enctype="multipart/form-data" action="{{ route('app::artigo::cadastra') }}" method="post">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ !isset($artigo->id) ? "" : $artigo->id }}">

                        {{--banner--}}
                        <div class="form-group">
                            <label for="banner">Banner</label>
                            <input type="file" class="form-control" id="banner" name="banner" title="Escolha o banner de apresentação do artigo">
                        </div>

                        {{--titulo--}}
                        <div class="form-group">
                            <label for="titulo">Titulo</label>
                            <input type="text" class="form-control" id="titulo" name="titulo" value="{{ !isset($artigo->titulo) ? "" : $artigo->titulo }}" placeholder="Escreva aqui o titulo do artigo" required>
                        </div>

                        {{--descrição curta--}}
                        <div class="form-group">
                            <label for="titulo">Descrição curta</label>
                            <textarea class="form-control" rows="4" maxlength="300" style="max-width: 100%; min-width: 100%;" id="conteudo_curto" name="conteudo_curto" placeholder="Escreva aqui uma descrição prévia do conteúdo" required>{{ !isset($artigo->conteudo_curto) ? "" : $artigo->conteudo_curto }}</textarea>
                        </div>

                        {{--conteudo--}}
                        <div class="form-group">
                            <label for="titulo">Conteúdo</label>
                            <textarea class="form-control" rows="4" style="max-width: 100%; min-width: 100%;" id="conteudo" name="conteudo" placeholder="Escreva aqui o conteudo do artigo" required>{{ !isset($artigo->conteudo) ? "" : $artigo->conteudo }}</textarea>
                        </div>

                        {{--seleção de tags--}}
                        <div class="form-group">
                            <label for="tags">Tags</label><br />
                            <select name="tags[]" id="tags" class="selectpicker" data-live-search="true" multiple title="Selecione as tags deste artigo" required>
                                @foreach ($tags as $tag)
                                    <option value="{{ $tag->id }}"
                                        @if (!empty($artigo))
                                            @foreach ($artigo->artigoTag as $artigoTag)
                                                {{ $artigoTag->tagId == $tag->id ? "selected" : "" }}
                                            @endforeach
                                        @endif
                                    >
                                        {{ $tag->nome }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        {{--destaque--}}
                        <div class="form-group">
                            <label for="destaque">Destaque?</label>
                            <input type="checkbox" name="destaque" id="destaque"
                                @if (!empty($artigo))
                                    {{ !empty($artigo->destaque) ? "checked" : "" }}
                                @endif
                                >
                        </div>

                        {{--botão enviar--}}
                        <div class="form-group">
                            <input type="submit" value="Enviar" class="btn btn-primary">
                        </div>

                    </form>
                    <!-- /.box-body -->
                </div>
            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Campo <strong>banner</strong> deve ser uma imagem</li>
                            <li>Campo <strong>Titulo</strong> é obrigatório</li>
                            <li>Campo <strong>Descrição</strong> curta é obrigatório. Este campo é apresentado na listagem de artigos</li>
                            <li>Campo <strong>Conteúdo</strong> é obrigatório.</li>
                            <li>Campo <strong>Tags</strong> é obrigatório.</li>
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>
        </section>
    </section>

<script>
</script>
@endsection