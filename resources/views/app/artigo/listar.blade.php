@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Listar
            <small>Artigo</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Artigo</a></li>
            <li class="active">Listar</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            <section class="col-md-12">
                <div class="box">

                    @include('flash::message')
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <th>Banner</th>
                                <th>Titulo</th>
                                <th>Descrição curta</th>
                                <th>Autor</th>
                                <th>Destaque</th>
                                <th>Status</th>
                                <th>Criado em</th>
                                <th>Ações</th>
                            </thead>
                            <tbody>
                                @foreach ($artigos as $artigo)
                                    <tr>
                                        <td>{{ asset($artigo->banner) }}</td>
                                        <td>{{ $artigo->titulo }}</td>
                                        <td>{{ $artigo->conteudo_curto }}</td>
                                        <td>{{ $artigo->writeAutor($artigo->usuarioAdmId) }}</td>
                                        <td>{{ $artigo->writeDestaque($artigo->destaque) }}</td>
                                        <td>{{ $artigo->writeStatus($artigo->status) }}</td>
                                        <td>{{ date('d/m/Y H\hi', strtotime($artigo->created_at)) }}</td>
                                        <td>
                                            <a href="{{ route('app::artigo::viewCadastra', ['id' => $artigo->id]) }}" class="btn btn-primary" style="color: #fff;">Editar</a>
                                            @if ($artigo->status == 0)
                                                <form action="{{ route('app::artigo::ativar') }}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="id" value="{{ $artigo->id }}">
                                                    <input type="submit" value="Ativar" class="btn btn-success">
                                                </form>
                                            @else
                                                <form action="{{ route('app::artigo::inativar') }}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="id" value="{{ $artigo->id }}">
                                                    <input type="submit" value="Inativar" class="btn btn-warning">
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="box-footer clearfix">
                            {{ $artigos->links() }}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </section>
        </section>
    </section>
@endsection