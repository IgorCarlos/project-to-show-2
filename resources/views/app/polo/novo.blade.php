@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Novo Polo
        </h1>

    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastro</h3>
                    </div>
                    @include('flash::message')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{route('app::polo::novo')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="titulo">Nome do polo</label>
                                    <input type="titulo" name="titulo" class="form-control" id="titulo" placeholder="Titulo">
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="imagem">Logo do polo</label>
                                    <input type="file" name="imagem" class="form-control" id="imagem" placeholder="imagem">
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>O campo titulo é obrigatório</li>
                            <li>A imagem é obrigatória e deve ter dimensão de: 1920x637 px</li>
                            <li>A imagem deve estar nas seguintes extensões: jpeg, png, bmp, gif, ou svg</li>
                        </ul>
                    </div>
                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection
