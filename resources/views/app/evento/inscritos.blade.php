@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Evento
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">

        <div class="col-md-12">
            <div class="row">

                <div class="box treeview ">
                    <div class="box-header with-border">
                        <h3 class="box-title" >Inscritos</h3>
                    </div>

                    <div class="form-group col-md-2">
                        <a href="{{route('app::evento::exportar', ['id' => $evento])}}"><button type="button" class="btn btn-block btn-info">Gerar planilha</button></a>
                    </div>
                    <div class="box-body">


                            <table class="table table-hover">
                                <thead>
                                <th>Dados</th>
                                <th>Unidade</th>
                                <th>Dia</th>
                                <th>Data cadastro</th>
                                <th>Confirmado</th>
                                </thead>
                                <tbody>
                                @foreach ($inscritos as $inscrito)

                                    <tr>
                                        <td>{{$inscrito->nome}} <br/> {{$inscrito->email}} <br/> {{$inscrito->documento}}</td>
                                        <td>{{$inscrito->unidades->nome}} <br/> {{$inscrito->unidades->endereco}} <br/> {{$inscrito->unidades->cidade}} - {{$inscrito->unidades->estado}}</td>
                                        <td>{{ date('d', strtotime($inscrito->dia)) }}</td>
                                        <td>{{ date('d/m/Y', strtotime($inscrito->created_at)) }}</td>
                                        <td>
                                            @if($inscrito->confirmado)
                                                <span class="label label-success">confirmado</span>
                                            @else
                                                <a href="{{route('app::evento::inscrito::confirmar', ['userId' => $inscrito->id])}}"><span class="label label-danger">confirmar</span></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>


                    </div>
                    <div class="box-footer clearfix">

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection