@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Novo Evento
        </h1>

    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastro</h3>
                    </div>
                @include('flash::message')
                <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('app::evento::novo')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">

                            <div class="form-group">
                                <label for="titulo">Titulo do evento</label>
                                <input type="titulo" name="titulo" class="form-control" id="titulo" placeholder="Titulo">
                            </div>
                            <div class="form-group">
                                <label for="titulo">Nome do palestrante</label>
                                <input type="titulo" name="nomepalestrante" class="form-control" id="palestrante" placeholder="Nome do palestrante">
                            </div>

                            <div class="form-group">
                                <label for="titulo">Descrição do palestrante</label>
                                <textarea type="titulo" name="descricaopalestrante" class="form-control" id="palestrante" placeholder="Descrição do palestrante"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="titulo">Link para video do palestrante</label>
                                <input type="titulo" name="linkvideo" class="form-control" id="palestrante" placeholder="Link para video do palestrante">
                            </div>

                            <div class="form-group">
                                <label>Quando irá começar o evento?</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" name="data1" class="form-control pull-right" id="reservation">
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label>Quando irá terminar o evento?</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" name="data2" class="form-control pull-right" id="reservation">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        {{--<ul>--}}
                            {{--<li>O campo titulo é obrigatório</li>--}}
                            {{--<li>A imagem é obrigatória e deve ter dimensão de: 1920x637 px</li>--}}
                            {{--<li>A imagem deve estar nas seguintes extensões: jpeg, png, bmp, gif, ou svg</li>--}}
                        {{--</ul>--}}
                    </div>
                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection
