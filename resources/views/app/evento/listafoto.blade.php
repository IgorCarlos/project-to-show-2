@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Evento
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">

        <div class="col-md-12">
            <div class="row">

                <div class="box treeview ">
                    <div class="box-header with-border">
                        <h3 class="box-title" >Fotos</h3>
                    </div>

                    @include('flash::message')

                    @include('shared.app.error')
                    <div class="box-body">
                        <form role="form" method="post" enctype="multipart/form-data" action="{{route('app::evento::addfotos')}}">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="descricao">Descrição da foto</label>
                                    <input type="text" class="form-control" name="descricao" placeholder="Descrição da foto">
                                </div>

                                <div class="form-group">
                                    <label for="descricao">Imagem</label>
                                    <input type="file" class="form-control" name="imagem" placeholder="">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Adicionar</button>
                            </div>
                        </form>

                            <table class="table table-hover">
                                <thead>
                                <th>Titulo</th>
                                <th>Imagem</th>

                                </thead>
                                <tbody>
                                @foreach ($fotos as $foto)

                                    <tr>
                                        <td>{{$foto->descricao}}</td>
                                        <td><img src="{{asset($foto->imagem)}}" alt=""/> </td>
                                        <td class="center">
                                            <!--<a data-toggle="modal" data-target="#modal-danger{{$foto->id}}" href="#"><i class="fa fa-trash-o fa-3" aria-hidden="true"></i></a>
-->
                                            <div class="modal modal-danger fade in" id="modal-danger{{$foto->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Confirmação</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Tem certeza que deseja deletar esse banner?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Fechar</button>
                                                            {{--<a href="{{route('app::evento::excluirMidia', ['id' => $foto->id])}}"><button type="button" class="btn btn-outline">Deletar</button></a>--}}
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                    </div>
                    <div class="box-footer clearfix">

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection