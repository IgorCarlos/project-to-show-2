@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Evento
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">

        <div class="col-md-12">
            <div class="row">

                    <div class="box treeview ">
                        <div class="box-header with-border">
                            <h3 class="box-title">Unidades</h3>

                            <div class="form-group col-md-2" style="float: right;">
                                <a href="{{route('app::unidade::viewNovo')}}"><button type="button" class="btn btn-block btn-info">Adicionar unidade</button></a>
                            </div>
                        </div>
                        <div class="box-body">

                            <form action="{{route('app::evento::unidades', ['id' => $idEvento])}}" method="post">
                                {{ csrf_field() }}
                                <table class="table table-hover">
                                    <thead>
                                        <th>Unidade</th>
                                        <th>Polo</th>
                                        <th>Endereco</th>
                                        <th>Datas</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($unidades as $unidade)

                                            <tr>
                                                <td>{{$unidade->nome}}</td>
                                                <td>{{$unidade->polo->nomePolo}}</td>
                                                <td>{{$unidade->cidade}}/{{$unidade->estado}}</td>
                                                <td>
                                                    @for ($i = $de; $i <= $ate; $i++)
                                                        <label for="">{{$i}}</label>
                                                        <?php
                                                            $selected = "";
                                                            if(count($unidade->dias) > 0){
                                                                if(in_array($i,$unidade->dias)){
                                                                    $selected = "checked";
                                                                }else{
                                                                    $selected = "";
                                                                }
                                                            }

                                                        ?>
                                                        <input type="checkbox" {{$selected}} name="unidade[{{$unidade->id}}][]" value="{{ $i }}">
                                                    @endfor
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                            </table>
                                <div class="form-group">
                                    <input type="submit" value="Enviar" class="btn btn-primary">
                                </div>
                            </form>
                        </div>
                        <div class="box-footer clearfix">

                        </div>
                    </div>

            </div>
        </div>
    </section>
@endsection