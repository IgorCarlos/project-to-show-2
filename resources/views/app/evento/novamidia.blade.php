@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Nova Noticia
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Banners</a></li>
            <li class="active">Novo</li>
        </ol>
    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastro</h3>
                    </div>
                @include('flash::message')
                <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('app::evento::addMidia')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">

                            <div class="form-group">
                                <label for="titulo">Titulo da noticia</label>
                                <input type="text" name="titulo" class="form-control" id="titulo" value="{{ old('titulo')  }}" placeholder="Titulo">
                            </div>

                            <div class="form-group">
                                <label for="link">Link para noticia</label>
                                <input type="text" name="link" class="form-control" id="link" value="{{ old('link')  }}" placeholder="Link">
                            </div>

                            <div class="form-group">
                                <label for="imagem">Link para noticia</label>
                                <input type="file" name="imagem" class="form-control" id="imagem" value="{{ old('imagem')  }}" placeholder="Link">
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        {{--<ul>--}}
                            {{--<li>O campo titulo é obrigatório</li>--}}
                            {{--<li>A imagem deve estar nas seguintes extensões: jpeg, png, bmp, gif, ou svg</li>--}}
                        {{--</ul>--}}
                    </div>
                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection
