@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Evento
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">

        <div class="col-md-12">
            <div class="row">

                <div class="box treeview ">
                    <div class="box-header with-border">
                        <h3 class="box-title" >Noticias</h3>
                    </div>

                    <div class="box-body">


                            <table class="table table-hover">
                                <thead>
                                <th>Titulo</th>
                                <th>Link</th>
                                <th>Excluir</th>
                                </thead>
                                <tbody>
                                @foreach ($midias as $midia)

                                    <tr>
                                        <td>{{$midia->veiculo}}</td>
                                        <td>{{$midia->linkNoticia}}</td>
                                        <td class="center">
                                            <a data-toggle="modal" data-target="#modal-danger{{$midia->id}}" href="#"><i class="fa fa-trash-o fa-3" aria-hidden="true"></i></a>

                                            <div class="modal modal-danger fade in" id="modal-danger{{$midia->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Confirmação</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Tem certeza que deseja deletar essa noticia?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Fechar</button>
                                                            <a href="{{route('app::evento::excluirMidia', ['id' => $midia->id])}}"><button type="button" class="btn btn-outline">Deletar</button></a>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                                        </td>
                                    </tr>
                
                                @endforeach
                                </tbody>

                            </table>


                    </div>
                    <div class="box-footer clearfix">

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection