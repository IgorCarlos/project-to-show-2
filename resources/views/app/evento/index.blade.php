@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Evento
        </h1>


    </section>
    <section class="content">

        <div class="col-md-12">
            <div class="row">
                @foreach($eventos as $evento)

                    <div class="box treeview ">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$evento->descricao}} - {{$evento->mes}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text"><a href="{{ route('app::evento::inscritos', ['id' => $evento->id]) }}">Inscritos</a></span>
                                        <span class="info-box-number">{{count($evento->inscritos)}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-4">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green"><i class="fa fa-building-o"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text"><a href="{{ route('app::polo::viewListar') }}">Polos</a></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <div class="col-md-4">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green"><i class="fa fa-building"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><a href="{{ route('app::evento::unidades', ['id' => $evento->id]) }}">Unidades</a></span>
                                        <span class="info-box-number">{{count($evento->unidades)}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </div>
                        <div class="box-footer clearfix">

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <a class="btn btn-block btn-primary btn-lg" href="{{route('app::evento::viewNovo')}}">Adicionar evento</a>
            </div>
        </div>
    </section>
@endsection