@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Novo Banner
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Banners</a></li>
            <li class="active">Novo</li>
        </ol>
    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastro</h3>
                    </div>
                    @include('flash::message')
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('app::trilha::editar',['id' => $trilha[0]->id])}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="titulo">Titulo</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" value="{{ $trilha[0]->titulo }}">
                            </div>

                            <div class="form-group">
                                <label for="descricao">Descrição</label>
                                <input type="text" class="form-control" id="descricao" name="descricao" value="{{ $trilha[0]->descricao }}">
                            </div>

                            @if($trilha[0]->id == 1)
                            <div class="form-group">
                                <label for="banner">Banner</label>
                                <input type="file" class="form-control" id="banner" name="banner">
                                <input type="hidden" class="form-control" id="banner" name="banner" value="{{ $trilha[0]->banner }}">
                            </div>
                            @endif

                            <div class="form-group">
                                <label for="questionario">Selecione um questionário para a trilha</label><br>
                                <select name="questionarioId" class="selectpicker">
                                    <option value="">Selecione</option>
                                @if(!empty($questionarios))

                                    @foreach($questionarios as $item)
                                        <option value="{{ $item->id }}"
                                            @if($trilha[0]->questionarioId == $item->id)
                                                {{ 'selected' }}
                                            @endif
                                        >
                                            {{ $item->titulo }}
                                        </option>
                                    @endforeach

                                @endif
                                </select>
                            </div>
                            
                            
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>O campo titulo é obrigatório</li>
                            <li>A imagem é obrigatória e deve ter dimensão de: 1920x637 px</li>
                            <li>A imagem deve estar nas seguintes extensões: jpeg, png, bmp, gif, ou svg</li>
                        </ul>
                    </div>
                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection