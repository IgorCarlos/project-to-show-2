@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Questionário
            <small>Trilha</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            @include('flash::message')
            <section class="col-md-8 tree menu-open" data-widget="tree">

                <div class="box">

                    <header class="box-header with-border">
                        @include('flash::message')
                    </header>

                    <div class="box-body">

                        <form action="{{ route('app::trilha::novoquestionario') }}" method="post">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label for="questionario_titulo">Titulo</label>
                                <input class="form-control" type="text" name="questionario_titulo" id="questionario_titulo" placeholder="Digite o titulo do questionário" required>
                            </div>

                            <div class="form-group">
                                <label for="questionario_descricao">Descrição</label>
                                <textarea name="questionario_descricao" id="questionario_descricao" rows="10" class="form-control" placeholder="Digite aqui a descrição do questionário"></textarea>
                            </div>


                            {{--questionario perguntas--}}
                            <div class="box-header with-border">
                                <h4>Perguntas do questionário</h4>
                            </div>

                            <div class="box-body">
                                <div class="js-perguntas">
                                    <div class="form-group">
                                        <label for="questionario_perguntas_descricao"></label>
                                        <input type="text" class="form-control numquest" name="questionario_perguntas_descricao[]" data-quest="1" id="questionario_perguntas_descricao" placeholder="Digite aqui a pergunta">
                                    </div>
                                </div>

                                <button type="button" class="btn btn-default add-more">Adicionar mais uma pergunta</button>
                            </div>

                            <div class="box-footer">
                                <input type="submit" value="Cadastrar" class="btn btn-success">
                            </div>

                        </form>
                        
                    </div>

                </div>

            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Caso queira excluir um banner, basta clicar no icone: <i class="fa fa-trash-o fa-3" aria-hidden="true"></i></li>
                            <li>Caso queira editar um banner, basta clicar no icone: <i class="fa fa-pencil" aria-hidden="true"></i></li>
                            <li>Caso queira desativar um banner clique em: <span class="label label-success">Ativo</span></li>
                            <li>Você pode também usar o campo de busca para encontrar um banner pelo titulo.</li>
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>
        </section>
    </section>

    <script>

        const localaddmore = document.getElementsByClassName('js-perguntas')[0];
        const elementaddmore = document.getElementsByClassName('add-more')[0];
        elementaddmore.addEventListener('click', addmore);

        function addmore () {
//        <div class="form-group">
//            <label for="questionario_perguntas_descricao">Pergunta 1</label>
//            <input type="text" class="form-control" name="questionario_perguntas_descricao[]" data-quest="1" id="questionario_perguntas_descricao" placeholder="Digite aqui a pergunta">
//        </div>

//            var totalquest = document.querySelectorAll('.numquest');

            var formgroup = document.createElement('div');
            formgroup.classList.add('form-group');

            var label = document.createElement('label');
//            label.textContent = 'Pergunta ' + (totalquest.length +1);


            var input = document.createElement('input');
            input.classList.add('form-control', 'numquest');
            input.name = 'questionario_perguntas_descricao[]';
            input.placeholder = 'Digite aqui a pergunta';

            formgroup.appendChild(label);
            formgroup.appendChild(input);

            localaddmore.appendChild(formgroup);

            // depois do elemento adicionado no dom, aplico foco | usabilidade
            input.focus();

        }

    </script>

    @endsection