@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Simple Tables
            <small>preview of simple tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            @include('flash::message')
            <section class="col-md-8 tree menu-open" data-widget="tree">
            
                @foreach($trilhas as $trilha)
                <div class="box treeview ">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $trilha->titulo }}</h3>
                        <a href="{{ route('app::trilha::editar', ['id' => $trilha->id]) }}" class="edit-trilha">Editar</a>
                    </div>
                </div>
                @endforeach
            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Caso queira excluir um banner, basta clicar no icone: <i class="fa fa-trash-o fa-3" aria-hidden="true"></i></li>
                            <li>Caso queira editar um banner, basta clicar no icone: <i class="fa fa-pencil" aria-hidden="true"></i></li>
                            <li>Caso queira desativar um banner clique em: <span class="label label-success">Ativo</span></li>
                            <li>Você pode também usar o campo de busca para encontrar um banner pelo titulo.</li>
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>
        </section>
    </section>


    @endsection