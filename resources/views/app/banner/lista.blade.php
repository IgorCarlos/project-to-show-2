@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Simple Tables
            <small>preview of simple tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">
        <section class="row">
            <section class="col-md-8">
                <div class="box">

                    @include('flash::message')
                    <div class="box-header with-border">
                        <h3 class="box-title">Bordered Table</h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table class="table table-hover">
                            <tbody>


                            <tr>
                                <th>Banner</th>
                                <th>Titulo</th>
                                <th>Status</th>
                                <th colspan="2"  class="center">Ações</th>
                            </tr>


                            @foreach($banners as $banner)

                                <tr>
                                    <td><img src="{{ asset($banner->imagem) }}" width="300" alt=""></td>
                                    <td>{{ $banner->titulo }}</td>
                                    <td>

                                        @if($banner->status_banner == 1)
                                            <a href="{{route('app::banner::status', ['id' => $banner->bannerId, 'status' => $banner->status_banner])}}"><span class="label label-success">Ativo</span></a>
                                        @else
                                            <a href="{{route('app::banner::status', ['id' => $banner->bannerId, 'status' => $banner->status_banner])}}"><span class="label label-danger">Inativo</span></a>
                                        @endif
                                    </td>
                                    <td class="center">
                                        <a data-toggle="modal" data-target="{{ '#modal-danger'.$banner->bannerId }}" href="#"><i class="fa fa-trash-o fa-3" aria-hidden="true"></i></a>

                                        <div class="modal modal-danger fade in" id="{{ 'modal-danger'.$banner->bannerId }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span></button>
                                                        <h4 class="modal-title">Confirmação</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Tem certeza que deseja deletar esse banner?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Fechar</button>
                                                        <a href="{{route('app::banner::deletar', ['id'=> $banner->bannerId])}}"><button type="button" class="btn btn-outline">Deletar</button></a>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                    </td>
                                    <td class="center">
                                        <a href=""><i class="fa fa-pencil fa-3" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">


                        {{ $banners->links() }}
                    </div>
                </div>
            </section>

            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>Caso queira excluir um banner, basta clicar no icone: <i class="fa fa-trash-o fa-3" aria-hidden="true"></i></li>
                            <li>Caso queira editar um banner, basta clicar no icone: <i class="fa fa-pencil" aria-hidden="true"></i></li>
                            <li>Caso queira desativar um banner clique em: <span class="label label-success">Ativo</span></li>
                            <li>Você pode também usar o campo de busca para encontrar um banner pelo titulo.</li>
                        </ul>
                    </div>
                    <div class="box-body">
                    </div>

                </div>
            </div>
        </section>
    </section>
@endsection