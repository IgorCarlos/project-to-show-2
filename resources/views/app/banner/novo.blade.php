@extends('layouts.app')


@section('content')
    <section class="content-header">
        <h1>
            Novo Banner
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Banners</a></li>
            <li class="active">Novo</li>
        </ol>
    </section>

    <section class="content">
        <section class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastro</h3>
                    </div>
                    @include('flash::message')
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('app::banner::novo')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="titulo">Titulo</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" value="{{ old('titulo') }}">
                            </div>
                            
                            <div class="form-group">
                                <label for="link">Link</label>
                                <input type="text" class="form-control" id="link" name="link" value="{{ old('link') }}">

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="novaguia" {{ (old('novaguia') == 'on') ? 'checked' : '' }}>
                                        Abrir link em nova guia?
                                    </label>
                                    <p class="help-block">Marque se o link deve ser aberto em nova guia.</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Imagem</label>
                                <input type="file" name="imagem" id="exampleInputFile" value="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Imagem Mobile</label>
                                <input type="file" name="imagemMobile" id="exampleInputFile" value="">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Instruções</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            <li>O campo titulo é obrigatório</li>
                            <li>A imagem é obrigatória e deve ter dimensão de: 1920x637 px</li>
                            <li>A imagem deve estar nas seguintes extensões: jpeg, png, bmp, gif, ou svg</li>
                        </ul>
                    </div>
                    <div class="box-body">
                        @include('shared.app.error')
                    </div>

                </div>
            </div>
        </section>
    </section>

@endsection